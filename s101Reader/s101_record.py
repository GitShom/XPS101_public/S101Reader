#!/usr/bin/env python
# -*- coding: UTF-8 -*-


#    Projet          : s101Reader
#    Version         : 1.0
#    Nom du fichier  : s101_record.py
#    Auteur          : Nicolas GABARRON, SHOM
#    Date            : 13/04/2022
#    Description     : Lecture d'une ENC au format S-101 - gestion d'un record

__author__ = "Nicolas GABARRON, SHOM"
__copyright__ = "Copyright 2022, s101Reader"
__credits__ = "Nicolas GABARRON"
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Nicolas GABARRON"
__email__ = "nicolas.gabarron@shom.fr"
__status__ = "Developpment"



from .s101_field import (s101_DSID, s101_DSSI, s101_ATCS, s101_ITCS, s101_FTCS,
                        s101_IACS, s101_FACS, s101_ARCS, s101_CSID, s101_CRSH,
                        s101_CSAX, s101_VDAT, s101_IRID, s101_ATTR, s101_INAS,
                        s101_PRID, s101_C2IT, s101_C3IT, s101_MRID, s101_C2IL,
                        s101_C3IL, s101_CRID, s101_PTAS, s101_SEGH, s101_CCID,
                        s101_CUCO, s101_SRID, s101_RIAS, s101_FRID, s101_FOID,
                        s101_SPAS, s101_FASC, s101_MASK)


ENDFIELD = 31   # code de fin de champ
ENDRECORD = 30  # code de fin d'enregistrement

DEBUG = True


class s101Record:

    def __init__(self, binary):
        self.binary = binary
        self.typeLabel = ''
        self.NameKey = -1

        self.leader = self.decodeLeader()
        self.directory = self.decodeDirectory(self.leader)


    def fieldContains(self, tag):
        # Teste si l'enregistrement contient le tag passé en paramètre
        for f in self.directory:
            if f['tag'] == tag:
                return True
        return False


    def tagInfos(self, tag):
        """
        Renvoie un tableau contenant les infos(position, longueur) des
        champs dont le tag passé en paramètre
        """
        infos = []
        for f in self.directory:
            if f['tag'] == tag:
                infos.append(f)
        return infos

    def binary_value(self, tag):
        # @tag : acronyme du champ
        t = self.tagInfos(tag)[0]
        p0 = self.leader['base_address_of_field_area'] + t['position']
        return self.binary[p0:p0+t['longueur']]


    def binary_value2(self, infos):
        # @infos : infos du champ
        p0 = self.leader['base_address_of_field_area'] + infos['position']
        return self.binary[p0:p0+infos['longueur']]


    def decode2(self, parent):

        if self.fieldContains('DSID'):
            for ti in self.tagInfos('DSID'):
                DSID = s101_DSID(self.binary_value2(ti)).decode(parent)
            for ti in self.tagInfos('DSSI'):
                DSSI = s101_DSSI(self.binary_value2(ti)).decode(DSID)
            if self.fieldContains('ATCS'):
                for ti in self.tagInfos('ATCS'):
                    ATCS = s101_ATCS(self.binary_value2(ti)).decode(DSID)
            if self.fieldContains('ITCS'):
                for ti in self.tagInfos('ITCS'):
                    ITCS = s101_ITCS(self.binary_value2(ti)).decode(DSID)
            if self.fieldContains('FTCS'):
                for ti in self.tagInfos('FTCS'):
                    FTCS = s101_FTCS(self.binary_value2(ti)).decode(DSID)
            if self.fieldContains('IACS'):
                for ti in self.tagInfos('IACS'):
                    IACS = s101_IACS(self.binary_value2(ti)).decode(DSID)
            if self.fieldContains('FACS'):
                for ti in self.tagInfos('FACS'):
                    FACS = s101_FACS(self.binary_value2(ti)).decode(DSID)
            if self.fieldContains('ARCS'):
                for ti in self.tagInfos('ARCS'):
                    ARCS = s101_ARCS(self.binary_value2(ti)).decode(DSID)

        if self.fieldContains('CSID'):
            for ti in self.tagInfos('CSID'):
                CSID = s101_CSID(self.binary_value2(ti)).decode(parent)

            i = 0
            while i < len(self.directory):
                if self.directory[i]['tag'] == 'CRSH':
                    CRSH = s101_CRSH(self.binary_value2(self.directory[i])).decode(CSID)
                    i += 1
                    if self.directory[i]['tag'] == 'CSAX':
                        CSAX = s101_CSAX(self.binary_value2(self.directory[i])).decode(CRSH)
                        i += 1
                        if self.directory[i]['tag'] == 'VDAT':
                            VDAT = s101_VDAT(self.binary_value2(self.directory[i])).decode(CRSH)
                            i += 1
                    elif self.directory[i]['tag'] == 'VDAT':
                        VDAT = s101_VDAT(self.binary_value2(self.directory[i])).decode(CRSH)
                        i += 1
                        if self.directory[i]['tag'] == 'CSAX':
                            CSAX = s101_CSAX(self.binary_value2(self.directory[i])).decode(CRSH)
                            i += 1
                else:
                    i += 1

        if self.fieldContains('IRID'):
            for ti in self.tagInfos('IRID'):
                IRID = s101_IRID(self.binary_value2(ti)).decode(parent)
            if self.fieldContains('ATTR'):
                for ti in self.tagInfos('ATTR'):
                    ATTR = s101_ATTR(self.binary_value2(ti)).decode(IRID)
            if self.fieldContains('INAS'):
                for ti in self.tagInfos('INAS'):
                    INAS = s101_INAS(self.binary_value2(ti)).decode(IRID)

        if self.fieldContains('PRID'):
            for ti in self.tagInfos('PRID'):
                PRID = s101_PRID(self.binary_value2(ti)).decode(parent)
            if self.fieldContains('INAS'):
                for ti in self.tagInfos('INAS'):
                    INAS = s101_INAS(self.binary_value2(ti)).decode(PRID)
            if self.fieldContains('C2IT'):
                for ti in self.tagInfos('C2IT'):
                    C2IT = s101_C2IT(self.binary_value2(ti)).decode(PRID)
            if self.fieldContains('C3IT'):
                for ti in self.tagInfos('C3IT'):
                    C3IT = s101_C3IT(self.binary_value2(ti)).decode(PRID)

        if self.fieldContains('MRID'):
            for ti in self.tagInfos('MRID'):
                MRID = s101_MRID(self.binary_value2(ti)).decode(parent)
            if self.fieldContains('INAS'):
                for ti in self.tagInfos('INAS'):
                    INAS = s101_INAS(self.binary_value2(ti)).decode(MRID)
            if self.fieldContains('C2IL'):
                for ti in self.tagInfos('C2IL'):
                    C2IL = s101_C2IL(self.binary_value2(ti)).decode(MRID)
            if self.fieldContains('C3IL'):
                for ti in self.tagInfos('C3IL'):
                    C3IL = s101_C3IL(self.binary_value2(ti)).decode(MRID)

        if self.fieldContains('CRID'):
            for ti in self.tagInfos('CRID'):
                CRID = s101_CRID(self.binary_value2(ti)).decode(parent)
            if self.fieldContains('INAS'):
                for ti in self.tagInfos('INAS'):
                    INAS = s101_INAS(self.binary_value2(ti)).decode(CRID)
            for ti in self.tagInfos('PTAS'):
                PTAS = s101_PTAS(self.binary_value2(ti)).decode(CRID)
            for ti in self.tagInfos('SEGH'):
                SEGH = s101_SEGH(self.binary_value2(ti)).decode(CRID)
            for ti in self.tagInfos('C2IL'):
                C2IL = s101_C2IL(self.binary_value2(ti)).decode(SEGH)

        if self.fieldContains('CCID'):
            for ti in self.tagInfos('CCID'):
                CCID = s101_CCID(self.binary_value2(ti)).decode(parent)
            if self.fieldContains('INAS'):
                for ti in self.tagInfos('INAS'):
                    INAS = s101_INAS(self.binary_value2(ti)).decode(CCID)
            if self.fieldContains('CUCO'):
                for ti in self.tagInfos('CUCO'):
                    CUCO = s101_CUCO(self.binary_value2(ti)).decode(CCID)

        if self.fieldContains('SRID'):
            for ti in self.tagInfos('SRID'):
                SRID = s101_SRID(self.binary_value2(ti)).decode(parent)
            if self.fieldContains('INAS'):
                for ti in self.tagInfos('INAS'):
                    INAS = s101_INAS(self.binary_value2(ti)).decode(SRID)
            if self.fieldContains('RIAS'):
                for ti in self.tagInfos('RIAS'):
                    RIAS = s101_RIAS(self.binary_value2(ti)).decode(SRID)

        if self.fieldContains('FRID'):
            for ti in self.tagInfos('FRID'):
                FRID = s101_FRID(self.binary_value2(ti)).decode(parent)
            for ti in self.tagInfos('FOID'):
                FOID = s101_FOID(self.binary_value2(ti)).decode(FRID)    
            if self.fieldContains('ATTR'):
                for ti in self.tagInfos('ATTR'):
                    ATTR = s101_ATTR(self.binary_value2(ti)).decode(FRID)
            if self.fieldContains('INAS'):
                for ti in self.tagInfos('INAS'):
                    INAS = s101_INAS(self.binary_value2(ti)).decode(FRID)
            if self.fieldContains('SPAS'):
                for ti in self.tagInfos('SPAS'):
                    SPAS = s101_SPAS(self.binary_value2(ti)).decode(FRID)
            if self.fieldContains('FASC'):
                for ti in self.tagInfos('FASC'):
                    FASC = s101_FASC(self.binary_value2(ti)).decode(FRID)
            if self.fieldContains('MASK'):
                for ti in self.tagInfos('MASK'):
                    MASK = s101_MASK(self.binary_value2(ti)).decode(FRID)


    """
    def decode(self):
        r = {}

        if self.fieldContains('DSID'):
            self.typeLabel = 'Dataset General Information record'
            self.tag = 'DSID'
            self.DSID = self.decodeField(self.tagInfos('DSID')[0])
            self.DSSI = self.decodeField(self.tagInfos('DSSI')[0])
            self.hasATCS = False
            if self.fieldContains('ATCS'):
                self.hasATCS = True
                self.ATCS = self.decodeField(self.tagInfos('ATCS')[0])
            if self.fieldContains('ITCS'):
                self.ITCS = self.decodeField(self.tagInfos('ITCS')[0])
            if self.fieldContains('FTCS'):
                self.FTCS = self.decodeField(self.tagInfos('FTCS')[0])
            if self.fieldContains('IACS'):
                self.IACS = self.decodeField(self.tagInfos('IACS')[0])
            if self.fieldContains('FACS'):
                self.FACS = self.decodeField(self.tagInfos('FACS')[0])
            if self.fieldContains('ARCS'):
                self.ARCS = self.decodeField(self.tagInfos('ARCS')[0])
            self.NameKey = int.from_bytes(self.DSID.binary[0:5], byteorder=sys.byteorder, signed=False)

        if self.fieldContains('IRID'):
            self.typeLabel = 'Information record'
            self.tag = 'IRID'
            self.IRID = self.decodeField(self.tagInfos('IRID')[0])

            if self.fieldContains('ATTR'):
                self.hasATTR = True
                self.ATTR = []
                inf = self.tagInfos('ATTR')
                for i in inf:
                    self.ATTR.append(self.decodeField(i))
            self.NameKey = int.from_bytes(self.IRID.binary[0:5], byteorder=sys.byteorder, signed=False)


        if self.fieldContains('PRID'):
            self.typeLabel = 'Point record'
            self.tag = 'PRID'
            self.PRID = self.decodeField(self.tagInfos('PRID')[0])
            self.hasINAS = False
            if self.fieldContains('INAS'):
                self.hasINAS = True
                self.INAS = []
                inf = self.tagInfos('INAS')
                for i in inf:
                    self.INAS.append(self.decodeField(i))
            if self.fieldContains('C2IT'):
                self.C2IT = self.decodeField(self.tagInfos('C2IT')[0])
                self.geometry = '2D'
            if self.fieldContains('C3IT'):
                self.C3IT = self.decodeField(self.tagInfos('C3IT')[0])
                self.geometry = '3D'
            self.NameKey = int.from_bytes(self.PRID.binary[0:5], byteorder=sys.byteorder, signed=False)


        if self.fieldContains('MRID'):
            self.typeLabel = 'Multi Point record'
            self.tag = 'MRID'
            self.MRID = self.decodeField(self.tagInfos('MRID')[0])
            if self.fieldContains('C2IL'):
                self.C2IL = []
                inf = self.tagInfos('C2IL')
                for i in inf:
                    self.C2IL.append(self.decodeField(i))
                #self.C2IL = self.decodeField(self.tagInfos('C2IL')[0])
                self.geometry = '2D'
            if self.fieldContains('C3IL'):
                self.C3IL = []
                inf = self.tagInfos('C3IL')
                for i in inf:
                    self.C3IL.append(self.decodeField(i))
                #self.C3IL = self.decodeField(self.tagInfos('C3IL')[0])
                self.geometry = '3D'
            self.NameKey = int.from_bytes(self.MRID.binary[0:5], byteorder=sys.byteorder, signed=False)


        if self.fieldContains('CRID'):
            self.typeLabel = 'Curve record'
            self.tag = 'CRID'
            self.CRID = self.decodeField(self.tagInfos('CRID')[0])
            if self.fieldContains('PTAS'):
                self.PTAS = self.decodeField(self.tagInfos('PTAS')[0])
            if self.fieldContains('C2IL'):
                self.C2IL = []
                inf = self.tagInfos('C2IL')
                for i in inf:
                    self.C2IL.append(self.decodeField(i))
            self.NameKey = int.from_bytes(self.CRID.binary[0:5], byteorder=sys.byteorder, signed=False)


        if self.fieldContains('CCID'):
            self.typeLabel = 'Composite Curve record'
            self.tag = 'CCID'
            self.CCID = self.decodeField(self.tagInfos('CCID')[0])
            self.NameKey = int.from_bytes(self.CCID.binary[0:5], byteorder=sys.byteorder, signed=False)


        if self.fieldContains('SRID'):
            self.typeLabel = 'Surface record'
            self.tag = 'SRID'
            self.SRID = self.decodeField(self.tagInfos('SRID')[0])
            self.RIAS = []
            inf = self.tagInfos('RIAS')
            for i in inf:
                self.RIAS.append(self.decodeField(i))
            self.NameKey = int.from_bytes(self.SRID.binary[0:5], byteorder=sys.byteorder, signed=False)

        if self.fieldContains('FRID'):
            self.typeLabel = 'Feature Type record'
            self.tag = 'FRID'
            self.FRID = self.decodeField(self.tagInfos('FRID')[0])

            self.FOID = self.decodeField(self.tagInfos('FOID')[0])

            self.hasATTR = False
            if self.fieldContains('ATTR'):
                self.hasATTR = True
                self.ATTR = []
                inf = self.tagInfos('ATTR')
                for i in inf:
                    self.ATTR.append(self.decodeField(i))

            self.hasSPAS = False
            if self.fieldContains('SPAS'):
                self.hasSPAS = True
                self.SPAS = []
                inf = self.tagInfos('SPAS')
                for i in inf:
                    self.SPAS.append(self.decodeField(i))
            self.NameKey = int.from_bytes(self.FRID.binary[0:5], byteorder=sys.byteorder, signed=False)

        return self
    """

    def decodeLeader(self):
        ''' Décode l'en-tête de l'enregistrement

        :return: dictionnaire des variables de l'en-tête

        .. code-block:: xml

            <DSSI AALL="1" DSTR="2" NALL="1" NOCN="3066" NOCR="0" NOED="4035" NOFA="0" NOGR="3048" NOIN="822" NOLR="2" NOMR="37"/>
        '''
        return {'record_length': int(self.binary[:5]), \
                'interchange_level': str(self.binary[5:6].decode('ascii')), \
                'leader_identifier': str(self.binary[6:7].decode('ascii')), \
                'inline_code_extension_indicator': str(self.binary[7:8].decode('ascii')), \
                'version_number': str(self.binary[8:9].decode('ascii')), \
                'application_indicator': str(self.binary[9:10].decode('ascii')), \
                'field_control_length': str(self.binary[10:12].decode('ascii')), \
                'base_address_of_field_area': int(self.binary[12:17]), \
                'extended_character_set_indicator': str(self.binary[17:20].decode('ascii')), \
                'size_of_field_length_field': int(self.binary[20:21]), \
                'size_of_field_position_field': int(self.binary[21:22]), \
                'reserved': str(self.binary[22:23].decode('ascii')), \
                'size_of_field_tag_field': int(self.binary[23:24])}


    def decodeDirectory(self, leader):
        ''' Décode le format (position, longueur) des champs présents dansl'enregistrement

        :param leader: en-tête de l'enregistrement
        :return: tableau des description des champs de l'en-tête
        '''
        tags = []

        size_tag = leader['size_of_field_tag_field']
        size_position = leader['size_of_field_position_field']
        size_length = leader['size_of_field_length_field']

        tag_length = size_tag + size_position + size_length
        nb = (leader['base_address_of_field_area'] - 1 - 24) / tag_length

        tt = self.binary[24:leader['base_address_of_field_area']-1]
        for j in range(int(nb)):
            m = (j) * tag_length
            tag = {'tag': str(tt[m:m+size_tag].decode('ascii')), \
                   'longueur': int(tt[m+size_tag:m+size_tag+size_length]), \
                   'position': int(tt[m+size_tag+size_length:m+size_tag+size_length+size_position])}

            tags.append(tag)

        return tags



    def decodeField(self, tag):
        p0 = self.leader['base_address_of_field_area'] + tag['position']
        binary_value = self.binary[p0:p0+tag['longueur']]

        if tag['tag'] == 'DSID':
            dd = s101_DSID(binary_value)
        if tag['tag'] == 'DSSI':
            dd = s101_DSSI(binary_value)
        if tag['tag'] == 'ATCS':
            dd = s101_ATCS(binary_value)
        if tag['tag'] == 'ITCS':
            dd = s101_ITCS(binary_value)
        if tag['tag'] == 'FTCS':
            dd = s101_FTCS(binary_value)
        if tag['tag'] == 'IACS':
            dd = s101_IACS(binary_value)
        if tag['tag'] == 'FACS':
            dd = s101_FACS(binary_value)
        if tag['tag'] == 'ARCS':
            dd = s101_ARCS(binary_value)
        if tag['tag'] == 'CSID':
            dd = s101_CSID(binary_value)
        if tag['tag'] == 'CRSH':
            dd = s101_CRSH(binary_value)
        if tag['tag'] == 'CSAX':
            dd = s101_CSAX(binary_value)
        if tag['tag'] == 'VDAT':
            dd = s101_VDAT(binary_value)
        if tag['tag'] == 'IRID':
            dd = s101_IRID(binary_value)
        if tag['tag'] == 'ATTR':
            dd = s101_ATTR(binary_value)
        if tag['tag'] == 'INAS':
            dd = s101_INAS(binary_value)
        if tag['tag'] == 'PRID':
            dd = s101_PRID(binary_value)
        if tag['tag'] == 'C2IT':
            dd = s101_C2IT(binary_value)
        if tag['tag'] == 'C3IT':
            dd = s101_C3IT(binary_value)

        if tag['tag'] == 'MRID':
            dd = s101_MRID(binary_value)
        if tag['tag'] == 'C2IL':
            dd = s101_C2IL(binary_value)
        if tag['tag'] == 'C3IL':
            dd = s101_C3IL(binary_value)

        if tag['tag'] == 'CRID':
            dd = s101_CRID(binary_value)

        if tag['tag'] == 'PTAS':
            dd = s101_PTAS(binary_value)
        if tag['tag'] == 'SEGH':
            dd = s101_SEGH(binary_value)
        if tag['tag'] == 'CCID':
            dd = s101_CCID(binary_value)
        if tag['tag'] == 'CUCO':
            dd = s101_CUCO(binary_value)
        if tag['tag'] == 'SRID':
            dd = s101_SRID(binary_value)
        if tag['tag'] == 'RIAS':
            dd = s101_RIAS(binary_value)
        if tag['tag'] == 'FRID':
            dd = s101_FRID(binary_value)
        if tag['tag'] == 'FOID':
            dd = s101_FOID(binary_value)
        if tag['tag'] == 'SPAS':
            dd = s101_SPAS(binary_value)
        if tag['tag'] == 'FASC':
            dd = s101_FASC(binary_value)
        if tag['tag'] == 'MASK':
            dd = s101_MASK(binary_value)

        return dd



if __name__ == '__main__':
    print("S-101 Record")