#!/usr/bin/env python
# -*- coding: UTF-8 -*-

#    Projet          : s101Reader
#    Version         : 1.0
#    Nom du fichier  : s101_field.py
#    Auteur          : Nicolas GABARRON, SHOM
#    Date            : 13/04/2022
#    Description     : Lecture d'une ENC au format S-101 - gestion d'un champ

__author__ = "Nicolas GABARRON, SHOM"
__copyright__ = "Copyright 2022, s101Reader"
__credits__ = "Nicolas GABARRON"
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Nicolas GABARRON"
__email__ = "nicolas.gabarron@shom.fr"
__status__ = "Developpment"



import sys
import struct

import xml.etree.cElementTree as ET

ENDFIELD = 31   # code de fin de champ
ENDRECORD = 30  # code de fin d'enregistrement

DEBUG = {'DSID': False, 'DSSI': False, 'ATCS': False, 'ITCS': False, 
         'FTCS': False, 'IACS': False, 'FACS': False, 'ARCS': False, 
         'CSID': False, 'CRSH': False, 'CSAX': False, 'VDAT': False, 
         'IRID': False, 'ATTR': False, 'INAS': False, 'PRID': False, 
         'C2IT': False, 'C3IT': False, 'MRID': False, 'C2IL': False, 
         'C3IL': False, 'CRID': False, 'PTAS': False, 'SEGH': False, 
         'CCID': False, 'CUCO': False, 'SRID': False, 'RIAS': False, 
         'FRID': False, 'FOID': False, 'SPAS': False, 'FASC': False, 
         'MASK': False}

class s101_DSID:
    ''' Lecture du champ Dataset Identification (DSID)

    :param binary: suite d'octets correspondant au champ DSID
    '''
    def __init__(self, binary):
        self.acronym = 'DSID'
        self.label = 'Dataset Identification'
        
        self.binary = binary

    def decode(self, parent):
        ''' Décode le champ DSID

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        RCNM = str(struct.unpack('B', self.binary[0:1])[0])
        RCID = str(struct.unpack('I', self.binary[1:5])[0])

        i = 5
        while ord(self.binary[i:i+1]) != ENDFIELD:
            i += 1
        ENSP = self.binary[5:i].decode('utf-8')
        i += 1
        j = i
        while ord(self.binary[j:j+1]) != ENDFIELD:
            j += 1
        ENED = self.binary[i:j].decode('utf-8')
        i = j + 1
        j = i
        while ord(self.binary[j:j+1]) != ENDFIELD:
            j += 1
        PRSP = self.binary[i:j].decode('utf-8')
        i = j + 1
        j = i
        while ord(self.binary[j:j+1]) != ENDFIELD:
            j += 1
        PRED = self.binary[i:j].decode('utf-8')
        i = j + 1
        j = i
        while ord(self.binary[j:j+1]) != ENDFIELD:
            j += 1
        PROF = self.binary[i:j].decode('utf-8')
        i = j + 1
        j = i
        while ord(self.binary[j:j+1]) != ENDFIELD:
            j += 1
        DSNM = self.binary[i:j].decode('utf-8')
        i = j + 1
        j = i
        while ord(self.binary[j:j+1]) != ENDFIELD:
            j += 1
        DSTL = self.binary[i:j].decode('utf-8')
        i = j + 1
        DSRD = self.binary[i:i+8].decode('utf-8')
        i = i + 8
        j = i
        while ord(self.binary[j:j+1]) != ENDFIELD:
            j += 1
        DSLG = self.binary[i:j].decode('utf-8')
        i = j + 1
        j = i
        while ord(self.binary[j:j+1]) != ENDFIELD:
            j += 1
        DSAB = self.binary[i:j].decode('utf-8')
        i = j + 1
        j = i
        while ord(self.binary[j:j+1]) != ENDFIELD:
            j += 1
        DSED = self.binary[i:j].decode('utf-8')

        parent.set('NameKey', str(int.from_bytes(self.binary[0:5], byteorder=sys.byteorder, signed=False)))
        DSID = ET.SubElement(parent, 'DSID', RCNM=RCNM,
                                             RCID=RCID,
                                             ENSP=ENSP,
                                             ENED=ENED,
                                             PRSP=PRSP,
                                             PRED=PRED,
                                             PROF=PROF,
                                             DSNM=DSNM,
                                             DSTL=DSTL,
                                             DSRD=DSRD,
                                             DSLG=DSLG,
                                             DSAB=DSAB,
                                             DSED=DSED)

        i = j + 1
        DSTC = ET.SubElement(DSID, "DSTC")
        while ord(self.binary[i:i+1]) != ENDFIELD and ord(self.binary[i:i+1]) != ENDRECORD:
            ET.SubElement(DSTC, "Categorie").text = str(struct.unpack('b', self.binary[i:i+1])[0])
            i += 1

        if DEBUG['DSID']: 
            print('Field DSID = ')
            ET.dump(DSID)
            
        return DSID
    
    

class s101_DSSI:
    ''' Lecture du champ Dataset Structure Information (DSSI)

    :param binary: suite d'octets correspondant au champ DSSI
    '''
    def __init__(self, binary):
        self.acronym = 'DSSI'
        self.label = 'Dataset Structure Information'
        self.binary = binary

    def decode(self, parent):
        ''' Décode le champ DSSI

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        DSSI = ET.SubElement(parent, 'DSSI', DCOX=str(struct.unpack('d', self.binary[0:8])[0]),
                                             DCOY=str(struct.unpack('d', self.binary[8:16])[0]),
                                             DCOZ=str(struct.unpack('d', self.binary[16:24])[0]),
                                             CMFX=str(struct.unpack('I', self.binary[24:28])[0]),
                                             CMFY=str(struct.unpack('I', self.binary[28:32])[0]),
                                             CMFZ=str(struct.unpack('I', self.binary[32:36])[0]),
                                             NOIR=str(struct.unpack('I', self.binary[36:40])[0]),
                                             NOPN=str(struct.unpack('I', self.binary[40:44])[0]),
                                             NOMN=str(struct.unpack('I', self.binary[44:48])[0]),
                                             NOCN=str(struct.unpack('I', self.binary[48:52])[0]),
                                             NOXN=str(struct.unpack('I', self.binary[52:56])[0]),
                                             NOSN=str(struct.unpack('I', self.binary[56:60])[0]),
                                             NOFR=str(struct.unpack('I', self.binary[60:64])[0]))

        if DEBUG['DSSI']: 
            print('Field DSSI = ')
            ET.dump(DSSI)
        return DSSI


class s101_ATCS:
    ''' Lecture du champ Attribute Codes (ATCS)

    :param binary: suite d'octets correspondant au champ ATCS
    '''
    def __init__(self, binary):
        self.acronym = 'ATCS'
        self.label = 'Attribute Codes'
        self.binary = binary
        self.codes = []

    def decode(self, parent):
        ''' Décode le champ ATCS

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        ATCS = ET.SubElement(parent, "ATCS")
        i = 0
        while ord(self.binary[i:i+1]) != ENDRECORD:
            j = i
            while ord(self.binary[j:j+1]) != ENDFIELD:
                j += 1
            ET.SubElement(ATCS, 'Code', ANCD=str(struct.unpack('h', self.binary[j+1:j+3])[0]),
                                        ATCD=self.binary[i:j].decode('utf-8'))
            i = j + 3
            
        if DEBUG['ATCS']: 
            print('Field ATCS = ')
            ET.dump(ATCS)
        return ATCS


    def getLabel(self, code):
        for c in self.codes:
            if c['ANCD'] == code:
                return c['ATCD']
        return "ATCS : code inconnu"


    def getCode(self, label):
        for c in self.codes:
            if c['ATCD'] == label:
                return c['ANCD']
        return "ATCS : label inconnu"



class s101_ITCS:
    ''' Lecture du champ Information Types Codes (ITCS)

    :param binary: suite d'octets correspondant au champ ITCS
    '''
    def __init__(self, binary):
        self.acronym = 'ITCS'
        self.label = 'Information Types Codes'
        self.binary = binary
        self.codes = []

    def decode(self, parent):
        ''' Décode le champ ITCS

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        ITCS = ET.SubElement(parent, "ITCS")
        i = 0
        while ord(self.binary[i:i+1]) != ENDRECORD:
            j = i
            while ord(self.binary[j:j+1]) != ENDFIELD:
                j += 1
            ET.SubElement(ITCS, 'Code', ITNC=str(struct.unpack('h', self.binary[j+1:j+3])[0]),
                                        ITCD=self.binary[i:j].decode('utf-8'))
            i = j + 3
        
        if DEBUG['ITCS']: 
            print('Field ITCS = ')
            ET.dump(ITCS)
        return ITCS


    def getLabel(self, code):
        for c in self.codes:
            if c['ITNC'] == code:
                return c['ITCD']
        return "ITCS : code inconnu"


    def getCode(self, label):
        for c in self.codes:
            if c['ITCD'] == label:
                return c['ITNC']
        return "ITCS : label inconnu"



class s101_FTCS:
    ''' Lecture du champ Feature Type Codes (FTCS)

    :param binary: suite d'octets correspondant au champ FTCS
    '''
    def __init__(self, binary):
        self.acronym = 'FTCS'
        self.label = 'Feature Type Codes'
        self.binary = binary
        self.codes = []

    def decode(self, parent):
        ''' Décode le champ FTCS

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        FTCS = ET.SubElement(parent, "FTCS")
        i = 0
        while ord(self.binary[i:i+1]) != ENDRECORD:
            j = i
            while ord(self.binary[j:j+1]) != ENDFIELD:
                j += 1
            ET.SubElement(FTCS, 'Code', FTNC=str(struct.unpack('h', self.binary[j+1:j+3])[0]),
                                        FTCD=self.binary[i:j].decode('utf-8'))
            i = j + 3
            
        if DEBUG['FTCS']: 
            print('Field FTCS = ')
            ET.dump(FTCS)
        return FTCS

    def getLabel(self, code):
        for c in self.codes:
            if c['FTNC'] == code:
                return c['FTCD']
        return "FTCS : code inconnu"


    def getCode(self, label):
        for c in self.codes:
            if c['FTCD'] == label:
                return c['FTNC']
        return "FTCS : label inconnu"



class s101_IACS:
    ''' Lecture du champ Information Association Codes (IACS)

    :param binary: suite d'octets correspondant au champ IACS
    '''
    def __init__(self, binary):
        self.acronym = 'IACS'
        self.label = 'Information Association Codes'
        self.binary = binary
        self.codes = []

    def decode(self, parent):
        ''' Décode le champ IACS

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        IACS = ET.SubElement(parent, "IACS")
        i = 0
        while ord(self.binary[i:i+1]) != ENDRECORD:
            j = i
            while ord(self.binary[j:j+1]) != ENDFIELD:
                j += 1
            ET.SubElement(IACS, 'Code', IANC=str(struct.unpack('h', self.binary[j+1:j+3])[0]),
                                        IACD=self.binary[i:j].decode('utf-8'))
            i = j + 3
            
        if DEBUG['IACS']: 
            print('Field IACS = ')
            ET.dump(IACS)
        return IACS


    def getLabel(self, code):
        for c in self.codes:
            if c['IANC'] == code:
                return c['IACD']
        return "IACS : code inconnu"


    def getCode(self, label):
        for c in self.codes:
            if c['IACD'] == label:
                return c['IANC']
        return "IACS : label inconnu"



class s101_FACS:
    ''' Lecture du champ Feature Association Codes (FACS)

    :param binary: suite d'octets correspondant au champ FACS
    '''
    def __init__(self, binary):
        self.acronym = 'FACS'
        self.label = 'Feature Association Codes'
        self.binary = binary
        self.codes = []

    def decode(self, parent):
        ''' Décode le champ FACS

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        FACS = ET.SubElement(parent, "FACS")
        i = 0
        while ord(self.binary[i:i+1]) != ENDRECORD:
            j = i
            while ord(self.binary[j:j+1]) != ENDFIELD:
                j += 1
            ET.SubElement(FACS, 'Code', FANC=str(struct.unpack('h', self.binary[j+1:j+3])[0]),
                                        FACD=self.binary[i:j].decode('utf-8'))
            i = j + 3
            
        if DEBUG['FACS']: 
            print('Field FACS = ')
            ET.dump(FACS)
        return FACS


    def getLabel(self, code):
        for c in self.codes:
            if c['FANC'] == code:
                return c['FACD']
        return "FACS : code inconnu"


    def getCode(self, label):
        for c in self.codes:
            if c['FACD'] == label:
                return c['FANC']
        return "FACSlabel inconnu"



class s101_ARCS:
    ''' Lecture du champ Association Role Codes (ARCS)

    :param binary: suite d'octets correspondant au champ ARCS
    '''
    def __init__(self, binary):
        self.acronym = 'ARCS'
        self.label = 'Association Role Codes'
        self.binary = binary
        self.codes = []

    def decode(self, parent):
        ''' Décode le champ ARCS

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        ARCS = ET.SubElement(parent, "ARCS")
        i = 0
        while ord(self.binary[i:i+1]) != ENDRECORD:
            j = i
            while ord(self.binary[j:j+1]) != ENDFIELD:
                j += 1
            ET.SubElement(ARCS, 'Code', ARNC=str(struct.unpack('h', self.binary[j+1:j+3])[0]),
                                        ARCD=self.binary[i:j].decode('utf-8'))
            i = j + 3
            
        if DEBUG['ARCS']: 
            print('Field ARCS = ')
            ET.dump(ARCS)
        return ARCS

    def getLabel(self, code):
        for c in self.codes:
            if c['ARNC'] == code:
                return c['ARCD']
        return "code inconnu"


    def getCode(self, label):
        for c in self.codes:
            if c['ARCD'] == label:
                return c['ARNC']
        return "label inconnu"



class s101_CSID:
    ''' Lecture du champ Coordinate Reference System Record Identifier (CSID)

    :param binary: suite d'octets correspondant au champ CSID
    '''
    def __init__(self, binary):
        self.acronym = 'CSID'
        self.label = 'Coordinate Reference System Record Identifier'
        self.binary = binary

    def decode(self, parent):
        ''' Décode le champ CSID

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        parent.set('NameKey', str(int.from_bytes(self.binary[0:5], byteorder=sys.byteorder, signed=False)))
        CSID = ET.SubElement(parent, 'CSID', RCNM=str(struct.unpack('B', self.binary[0:1])[0]),
                                             RCID=str(struct.unpack('I', self.binary[1:5])[0]),
                                             NCRC=str(struct.unpack('B', self.binary[5:6])[0]))
        
        if DEBUG['CSID']: 
            print('Field CSID = ')
            ET.dump(CSID)
        return CSID



class s101_CRSH:
    ''' Lecture du champ Coordinate Reference System Header (CRSH)

    :param binary: suite d'octets correspondant au champ CRSH
    '''
    def __init__(self, binary):
        self.acronym = 'CRSH'
        self.label = 'Coordinate Reference System Header'
        self.binary = binary

    def decode(self, parent):
        ''' Décode le champ CRSH

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        #CRSH = ET.SubElement(parent, "CRSH")
        CRIX = str(struct.unpack('B', self.binary[0:1])[0])
        CRST = str(struct.unpack('B', self.binary[1:2])[0])
        CSTY = str(struct.unpack('B', self.binary[2:3])[0])

        i = 3
        while ord(self.binary[i:i+1]) != ENDFIELD:
            i += 1
        #CRNM = "".join( chr(x) for x in self.binary[3:i])
        CRNM = self.binary[3:i].decode('utf-8')
        i += 1
        j = i
        while ord(self.binary[j:j+1]) != ENDFIELD:
            j += 1
        #CRSI = "".join( chr(x) for x in self.binary[i:j])
        CRSI = self.binary[i:j].decode('utf-8')
        i = j + 1
        self.CRSS = int.from_bytes(self.binary[i:i+1], byteorder=sys.byteorder, signed=False)
        CRSS = str(struct.unpack('B', self.binary[i:i+1])[0])

        CRSH = ET.SubElement(parent, 'CRSH', CRIX=CRIX,
                                             CRST=CRST,
                                             CSTY=CSTY,
                                             CRNM=CRNM,
                                             CRSI=CRSI,
                                             CRSS=CRSS)

        if DEBUG['CRSH']: 
            print('Field CRSH = ')
            ET.dump(CRSH)
        return CRSH



class s101_CSAX:
    ''' Lecture du champ Coordinate System Axes (CSAX)

    :param binary: suite d'octets correspondant au champ CSAX
    '''
    def __init__(self, binary):
        self.acronym = 'CSAX'
        self.label = 'Coordinate System Axes'
        self.binary = binary


    def decode(self, parent):
        ''' Décode le champ CSAX

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        CSAX = ET.SubElement(parent, "CSAX")
        i = 0
        while ord(self.binary[i:i+1]) != ENDRECORD:
            AXTY = str(struct.unpack('B', self.binary[i:i+1])[0])
            AXUM = str(struct.unpack('B', self.binary[i+1:i+2])[0])

            ET.SubElement(CSAX, 'Axis', AXTY=AXTY,
                                        AXUM=AXUM)
            i += 2

        if DEBUG['CSAX']: 
            print('Field CSAX = ')
            ET.dump(CSAX)
        return CSAX



class s101_VDAT:
    ''' Lecture du champ Vertical Datum (VDAT)

    :param binary: suite d'octets correspondant au champ VDAT
    '''
    def __init__(self, binary):
        self.acronym = 'VDAT'
        self.label = 'Vertical Datum'
        self.binary = binary


    def decode(self, parent):
        ''' Décode le champ VDAT

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        i = 0
        while ord(self.binary[i:i+1]) != ENDFIELD:
            i += 1
        #DTNM = "".join( chr(x) for x in self.binary[0:i])
        DTNM = self.binary[0:i].decode('utf-8')

        i += 1
        j = i
        while ord(self.binary[j:j+1]) != ENDFIELD:
            j += 1
        #DTID = "".join( chr(x) for x in self.binary[i:j])
        DTID = self.binary[i:j].decode('utf-8')

        i = j + 1
        DTSR = str(struct.unpack('B', self.binary[i:i+1])[0])
        i += 1
        j = i
        while ord(self.binary[j:j+1]) != ENDFIELD:
            j += 1
        #SCRI = "".join( chr(x) for x in self.binary[i:j])
        SCRI = self.binary[i:j].decode('utf-8')

        VDAT = ET.SubElement(parent, 'VDAT', DTNM=DTNM,
                                             DTID=DTID,
                                             DTSR=DTSR,
                                             SCRI=SCRI)

        if DEBUG['VDAT']: 
            print('Field VDAT = ')
            ET.dump(VDAT)
        return VDAT


class s101_IRID:
    ''' Lecture du champ Information Type Identifier (IRID)

    :param binary: suite d'octets correspondant au champ IRID
    '''
    def __init__(self, binary):
        self.acronym = 'IRID'
        self.label = 'Information Type Record Identifier'
        self.binary = binary

    def decode(self, parent):
        ''' Décode le champ IRID

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        parent.set('NameKey', str(int.from_bytes(self.binary[0:5], byteorder=sys.byteorder, signed=False)))
        IRID = ET.SubElement(parent, 'IRID', RCNM=str(struct.unpack('B', self.binary[0:1])[0]),
                                             RCID=str(struct.unpack('I', self.binary[1:5])[0]),
                                             NITC=str(struct.unpack('h', self.binary[5:7])[0]),
                                             RVER=str(struct.unpack('h', self.binary[7:9])[0]),
                                             RUIN=str(struct.unpack('B', self.binary[9:10])[0]))
        if DEBUG['IRID']: 
            print('Field IRID = ')
            ET.dump(IRID)
        return IRID


class s101_ATTR:
    ''' Lecture du champ Attribute (ATTR)

    :param binary: suite d'octets correspondant au champ ATTR
    '''
    def __init__(self, binary):
        self.acronym = 'ATTR'
        self.label = 'Attribute'
        self.binary = binary

    def decode(self, parent):
        ''' Décode le champ ATTR et recrée l'arborescence des attributs dans le cas des attributs complexes

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        """
        ATTR = ET.SubElement(parent, "ATTR")
        i = 0
        while ord(self.binary[i:i+1]) != ENDRECORD:
            j = i
            NATC = str(struct.unpack('h', self.binary[j:j+2])[0])
            ATIX = str(struct.unpack('h', self.binary[j+2:j+4])[0])
            PAIX = str(struct.unpack('h', self.binary[j+4:j+6])[0])
            ATIN = str(struct.unpack('B', self.binary[j+6:j+7])[0])
            j += 7
            k = j
            while ord(self.binary[k:k+1]) != ENDFIELD:
                k += 1
            ET.SubElement(ATTR, 'Attribute', NATC=NATC,
                                             ATIX=ATIX,
                                             PAIX=PAIX,
                                             ATIN=ATIN,
                                             ATVL="".join( chr(x) for x in self.binary[j:k]))
            i = k + 1
            
        return ATTR
        """
        ATTR = ET.Element('Attributes')
        i = 0
        while ord(self.binary[i:i+1]) != ENDRECORD:
            j = i
            NATC = str(struct.unpack('h', self.binary[j:j+2])[0])
            ATIX = str(struct.unpack('h', self.binary[j+2:j+4])[0])
            PAIX = str(struct.unpack('h', self.binary[j+4:j+6])[0])
            ATIN = str(struct.unpack('B', self.binary[j+6:j+7])[0])
            j += 7
            k = j
            while ord(self.binary[k:k+1]) != ENDFIELD:
                k += 1
            ET.SubElement(ATTR, 'Attribute', NATC=NATC,
                                             ATIX=ATIX,
                                             PAIX=PAIX,
                                             ATIN=ATIN,
                                             ATVL=self.binary[j:k].decode('utf-8'))
            i = k + 1
            
        nbattr = 0
        for a in ATTR.findall("./Attribute"):
            nbattr += 1
            a.set('id', str(nbattr))
            
        #print('nb attr', i)
        #print(ET.dump(ATTR))
        
        treeATTR = ET.Element('ATTR')
        
        ids = []            
        for a in ATTR.findall("./Attribute"):
            if a.get('PAIX') == '0':
                a.set('checked', '1')
                treeATTR.insert(1, a)
                ids.append(a.get('id'))
            else:
                a.set('checked', '0')
                #pass
                
        #print(ET.dump(treeATTR))
        
        #print('nb attr', nbattr, '  ----    ids', len(ids))
        #print(ids)
        ##print('ids', len(ids))
        while len(ids) != nbattr:
            #print('nb attr', nbattr, '  ----    ids', len(ids))
            for a in ATTR.findall("./Attribute"):
                #print(ET.dump(a))
                if a.get('checked') == '0' and a.get('PAIX') in ids:
                    #print('plouf', a.get('PAIX'))
                    #treeATTR.find("./Attribute[@id='"+a.get('PAIX')+"']").insert(1, a)
                    f = treeATTR.find(".//Attribute[@id='"+a.get('PAIX')+"']")
                    if f is not None:
                        a.set('checked', '1')
                        f.insert(1, a)
                        ids.append(a.get('id'))
                    #print(ET.dump(a))
                    #print(ids)
        
        #print('#############')
        for a in treeATTR.findall(".//Attribute"):
            del a.attrib["id"]
            del a.attrib["checked"]
            #del a.attrib["ATIN"]
            #del a.attrib["ATIX"]
            #del a.attrib["PAIX"]
            
        parent.insert(1, treeATTR)
        
            
        #ET.SubElement(parent, treeATTR)
        
        if DEBUG['ATTR']: 
            print('Field ATTR = ')
            ET.dump(treeATTR)
        
        return treeATTR
        
        

class s101_INAS:
    ''' Lecture du champ Information Association (INAS)

    :param binary: suite d'octets correspondant au champ INAS
    '''
    def __init__(self, binary):
        self.acronym = 'INAS'
        self.label = 'Information Association'
        self.binary = binary

    def decode(self, parent):
        ''' Décode le champ INAS

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''

        nk = str(int.from_bytes(self.binary[0:5], byteorder=sys.byteorder, signed=False))
        
        RCNM = str(struct.unpack('B', self.binary[0:1])[0])
        RCID = str(struct.unpack('I', self.binary[1:5])[0])
        NIAC = str(struct.unpack('h', self.binary[5:7])[0])
        NARC = str(struct.unpack('h', self.binary[7:9])[0])
        IUIN = str(struct.unpack('B', self.binary[9:10])[0])
        
        INAS = ET.SubElement(parent, 'INAS', NameKey=nk,
                                             RCNM=RCNM,
                                             RCID=RCID,
                                             NIAC=NIAC,
                                             NARC=NARC,
                                             IUIN=IUIN)
        
        #AT = s101_ATTR(self.binary[10:])
        #print(self.binary[10:])
        ATTR = s101_ATTR(self.binary[10:]).decode(INAS)
        
        if DEBUG['INAS']: 
            print('Field INAS = ')
            ET.dump(INAS)
        return INAS



class s101_PRID:
    ''' Lecture du champ Point Record Identifier (PRID)

    :param binary: suite d'octets correspondant au champ PRID
    '''
    def __init__(self, binary):
        self.acronym = 'PRID'
        self.label = 'Point Record Identifier'
        self.binary = binary

    def decode(self, parent):
        ''' Décode le champ PRID

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        parent.set('NameKey', str(int.from_bytes(self.binary[0:5], byteorder=sys.byteorder, signed=False)))
        PRID = ET.SubElement(parent, 'PRID', RCNM=str(struct.unpack('B', self.binary[0:1])[0]),
                                             RCID=str(struct.unpack('I', self.binary[1:5])[0]),
                                             RVER=str(struct.unpack('h', self.binary[5:7])[0]),
                                             RUIN=str(struct.unpack('B', self.binary[7:8])[0]))
        if DEBUG['PRID']: 
            print('Field PRID = ')
            ET.dump(PRID)
        return PRID



class s101_C2IT:
    ''' Lecture du champ 2-D Integer Coordinate Tuple (C2IT)

    :param binary: suite d'octets correspondant au champ C2IT
    '''
    def __init__(self, binary):
        self.acronym = 'C2IT'
        self.label = '2-D Integer Coordinate Tuple'
        self.binary = binary

    def decode(self, parent):
        ''' Décode le champ C2IT

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        C2IT = ET.SubElement(parent, 'C2IT', YCOO=str(struct.unpack('l', self.binary[0:4])[0]),
                                             XCOO=str(struct.unpack('l', self.binary[4:8])[0]))
        if DEBUG['C2IT']: 
            print('Field C2IT = ')
            ET.dump(C2IT)
        return C2IT


class s101_C3IT:
    ''' Lecture du champ 3-D Integer Coordinate Tuple (C3IT)

    :param binary: suite d'octets correspondant au champ C3IT
    '''
    def __init__(self, binary):
        self.acronym = 'C3IT'
        self.label = '3-D Integer Coordinate Tuple'
        self.binary = binary

    def decode(self, parent):
        ''' Décode le champ C3IT

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        C3IT = ET.SubElement(parent, 'C3IT', VCID=str(struct.unpack('B', self.binary[0:1])[0]),
                                             YCOO=str(struct.unpack('l', self.binary[1:5])[0]),
                                             XCOO=str(struct.unpack('l', self.binary[5:9])[0]),
                                             ZCOO=str(struct.unpack('l', self.binary[9:13])[0]))
        if DEBUG['C3IT']: 
            print('Field C3IT = ')
            ET.dump(C3IT)
        return C3IT


class s101_MRID:
    ''' Lecture du champ Multi Point Record Identifier (MRID)

    :param binary: suite d'octets correspondant au champ MRID
    '''
    def __init__(self, binary):
        self.acronym = 'MRID'
        self.label = 'Multi Point Record Identifier'
        self.binary = binary

    def decode(self, parent):
        ''' Décode le champ MRID

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        parent.set('NameKey', str(int.from_bytes(self.binary[0:5], byteorder=sys.byteorder, signed=False)))
        MRID = ET.SubElement(parent, 'MRID', RCNM=str(struct.unpack('B', self.binary[0:1])[0]),
                                             RCID=str(struct.unpack('I', self.binary[1:5])[0]),
                                             RVER=str(struct.unpack('h', self.binary[5:7])[0]),
                                             RUIN=str(struct.unpack('B', self.binary[7:8])[0]))
        if DEBUG['MRID']: 
            print('Field MRID = ')
            ET.dump(MRID)
        return MRID


class s101_C2IL:
    ''' Lecture du champ 2-D Integer Coordinate List (C2IL)

    :param binary: suite d'octets correspondant au champ C2IL
    '''
    def __init__(self, binary):
        self.acronym = 'C2IL'
        self.label = '2-D Integer Coordinate List'
        #self.binary = binary
        self.binary = binary[:-1]
        self.points = []

    def decode(self, parent):
        ''' Décode le champ C2IL

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        C2IL = ET.SubElement(parent, "C2IL")
        i = 0
        while i < len(self.binary):
            ET.SubElement(C2IL, 'Point', YCOO=str(struct.unpack('l', self.binary[i:i+4])[0]),
                                         XCOO=str(struct.unpack('l', self.binary[i+4:i+8])[0]))
            i += 8
        
        if DEBUG['C2IL']: 
            print('Field C2IL = ')
            ET.dump(C2IL)
        return C2IL


class s101_C3IL:
    #  field
    ''' Lecture du champ 3-D Integer Coordinate List (C3IL)

    :param binary: suite d'octets correspondant au champ C3IL
    '''
    def __init__(self, binary):
        self.acronym = 'C3IL'
        self.label = '3-D Integer Coordiante List'
        #self.binary = binary
        self.binary = binary[:-1]
        self.points = []

    def decode(self, parent):
        ''' Décode le champ C3IL

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        C3IL = ET.SubElement(parent, 'C3IL', VCID=str(struct.unpack('B', self.binary[0:1])[0]))
        i = 1
        while i < len(self.binary):
            ET.SubElement(C3IL, 'Point', YCOO=str(struct.unpack('l', self.binary[i:i+4])[0]),
                                         XCOO=str(struct.unpack('l', self.binary[i+4:i+8])[0]),
                                         ZCOO=str(struct.unpack('l', self.binary[i+8:i+12])[0]))

            i += 12
        
        if DEBUG['C3IL']: 
            print('Field C3IL = ')
            ET.dump(C3IL)
        return C3IL


class s101_CRID:
    ''' Lecture du champ Curve Record Identifier (CRID)

    :param binary: suite d'octets correspondant au champ CRID
    '''
    def __init__(self, binary):
        self.acronym = 'CRID'
        self.label = 'Curve Record Identifier'
        self.binary = binary

    def decode(self, parent):
        ''' Décode le champ CRID

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        parent.set('NameKey', str(int.from_bytes(self.binary[0:5], byteorder=sys.byteorder, signed=False)))
        CRID = ET.SubElement(parent, 'CRID', RCNM=str(struct.unpack('B', self.binary[0:1])[0]),
                                             RCID=str(struct.unpack('I', self.binary[1:5])[0]),
                                             RVER=str(struct.unpack('h', self.binary[5:7])[0]),
                                             RUIN=str(struct.unpack('B', self.binary[7:8])[0]))
        return CRID


class s101_PTAS:
    ''' Lecture du champ Point Association (PTAS)

    :param binary: suite d'octets correspondant au champ PTAS
    '''
    def __init__(self, binary):
        self.acronym = 'PTAS'
        self.label = 'Point Association'
        self.binary = binary
        self.points = []

    def decode(self, parent):
        ''' Décode le champ PTAS

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        PTAS = ET.SubElement(parent, "PTAS")
        i = 0
        while ord(self.binary[i:i+1]) != ENDRECORD:
            ET.SubElement(PTAS, 'PointAssociation', NameKey=str(int.from_bytes(self.binary[i:i+5], byteorder=sys.byteorder, signed=False)),
                                                    RRNM=str(struct.unpack('B', self.binary[i:i+1])[0]),
                                                    RRID=str(struct.unpack('I', self.binary[i+1:i+5])[0]),
                                                    TOPI=str(struct.unpack('B', self.binary[i+5:i+6])[0]))
            i += 6
        return PTAS


class s101_SEGH:
    ''' Lecture du champ Segment Header (SEGH)

    :param binary: suite d'octets correspondant au champ SEGH
    '''
    def __init__(self, binary):
        self.acronym = 'SEGH'
        self.label = 'Segment Header'
        self.binary = binary

    def decode(self, parent):
        ''' Décode le champ SEGH

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        SEGH = ET.SubElement(parent, 'SEGH', INTP=str(struct.unpack('B', self.binary[0:1])[0]))
        return SEGH


class s101_CCID:
    ''' Lecture du champ Composite Curve Record Identifier (CCID)

    :param binary: suite d'octets correspondant au champ CCID
    '''
    def __init__(self, binary):
        self.acronym = 'CCID'
        self.label = 'Composite Curve Record Identifier'
        self.binary = binary

    def decode(self, parent):
        ''' Décode le champ CCID

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        parent.set('NameKey', str(int.from_bytes(self.binary[0:5], byteorder=sys.byteorder, signed=False)))
        CCID = ET.SubElement(parent, 'CCID', RCNM=str(struct.unpack('B', self.binary[0:1])[0]),
                                             RCID=str(struct.unpack('I', self.binary[1:5])[0]),
                                             RVER=str(struct.unpack('h', self.binary[5:7])[0]),
                                             RUIN=str(struct.unpack('B', self.binary[7:8])[0]))
        return CCID


class s101_CUCO:
    ''' Lecture du champ Curve Component (CUCO)

    :param binary: suite d'octets correspondant au champ CUCO
    '''
    def __init__(self, binary):
        self.acronym = 'CUCO'
        self.label = 'Curve Component'
        self.binary = binary

    def decode(self, parent):
        ''' Décode le champ CUCO

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        CUCO = ET.SubElement(parent, "CUCO")
        i = 0
        while ord(self.binary[i:i+1]) != ENDRECORD:
            ET.SubElement(CUCO, 'Component', NameKey=str(int.from_bytes(self.binary[i:i+5], byteorder=sys.byteorder, signed=False)),
                                             RRNM=str(struct.unpack('B', self.binary[i:i+1])[0]),
                                             RRID=str(struct.unpack('I', self.binary[i+1:i+5])[0]),
                                             ORNT=str(struct.unpack('B', self.binary[i+5:i+6])[0]))
            i += 6
        return CUCO


class s101_SRID:
    ''' Lecture du champ Surface Record Identifier (SRID)

    :param binary: suite d'octets correspondant au champ SRID
    '''
    def __init__(self, binary):
        self.acronym = 'SRID'
        self.label = 'Surface Record Identifier'
        self.binary = binary

    def decode(self, parent):
        ''' Décode le champ SRID

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        parent.set('NameKey', str(int.from_bytes(self.binary[0:5], byteorder=sys.byteorder, signed=False)))
        SRID = ET.SubElement(parent, 'SRID', RCNM=str(struct.unpack('B', self.binary[0:1])[0]),
                                             RCID=str(struct.unpack('I', self.binary[1:5])[0]),
                                             RVER=str(struct.unpack('h', self.binary[5:7])[0]),
                                             RUIN=str(struct.unpack('B', self.binary[7:8])[0]))
        return SRID


class s101_RIAS:
    ''' Lecture du champ Ring Association (RIAS)

    :param binary: suite d'octets correspondant au champ RIAS
    '''
    def __init__(self, binary):
        self.acronym = 'RIAS'
        self.label = 'Ring Association'
        self.binary = binary

    def decode(self, parent):
        ''' Décode le champ RIAS

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        RIAS = ET.SubElement(parent, "RIAS")
        i = 0
        while ord(self.binary[i:i+1]) != ENDRECORD:
            ET.SubElement(RIAS, 'Ring', NameKey=str(int.from_bytes(self.binary[i:i+5], byteorder=sys.byteorder, signed=False)),
                                        RRNM=str(struct.unpack('B', self.binary[i:i+1])[0]),
                                        RRID=str(struct.unpack('I', self.binary[i+1:i+5])[0]),
                                        ORNT=str(struct.unpack('B', self.binary[i+5:i+6])[0]),
                                        USAG=str(struct.unpack('B', self.binary[i+6:i+7])[0]),
                                        RAUI=str(struct.unpack('B', self.binary[i+7:i+8])[0]))
            i += 8
        return RIAS


class s101_FRID:
    ''' Lecture du champ Feature Type Record Identifier (FRID)

    :param binary: suite d'octets correspondant au champ FRID
    '''
    def __init__(self, binary):
        self.acronym = 'FRID'
        self.label = 'Feature Type Record Identifier'
        self.binary = binary

    def decode(self, parent):
        ''' Décode le champ FRID

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        parent.set('NameKey', str(int.from_bytes(self.binary[0:5], byteorder=sys.byteorder, signed=False)))
        FRID = ET.SubElement(parent, 'FRID', RCNM=str(struct.unpack('B', self.binary[0:1])[0]),
                                             RCID=str(struct.unpack('I', self.binary[1:5])[0]),
                                             NFTC=str(struct.unpack('h', self.binary[5:7])[0]),
                                             RVER=str(struct.unpack('h', self.binary[7:9])[0]),
                                             RUIN=str(struct.unpack('B', self.binary[9:10])[0]))
        return FRID


class s101_FOID:
    ''' Lecture du champ Feature Object Identifier (FOID)

    :param binary: suite d'octets correspondant au champ FOID
    '''
    def __init__(self, binary):
        self.acronym = 'FOID'
        self.label = 'Feature Object Identifier'
        self.binary = binary

    def decode(self, parent):
        ''' Décode le champ FOID

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        FOID = ET.SubElement(parent, 'FOID', AGEN=str(struct.unpack('h', self.binary[0:2])[0]),
                                             FIDN=str(struct.unpack('I', self.binary[2:6])[0]),
                                             FIDS=str(struct.unpack('h', self.binary[6:8])[0]))
        return FOID


class s101_SPAS:
    ''' Lecture du champ Spatial Association (SPAS)

    :param binary: suite d'octets correspondant au champ SPAS
    '''
    def __init__(self, binary):
        self.acronym = 'SPAS'
        self.label = 'Spatial Association'
        self.binary = binary

    def decode(self, parent):
        ''' Décode le champ SPAS

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        SPAS = ET.SubElement(parent, "SPAS")
        i = 0
        while ord(self.binary[i:i+1]) != ENDRECORD:
            ET.SubElement(SPAS, 'Association', NameKey=str(int.from_bytes(self.binary[i:i+5], byteorder=sys.byteorder, signed=False)),
                                               RRNM=str(struct.unpack('B', self.binary[i:i+1])[0]),
                                               RRID=str(struct.unpack('I', self.binary[i+1:i+5])[0]),
                                               ORNT=str(struct.unpack('B', self.binary[i+5:i+6])[0]),
                                               SMIN=str(struct.unpack('I', self.binary[i+6:i+10])[0]),
                                               SMAX=str(struct.unpack('I', self.binary[i+10:i+14])[0]),
                                               SAUI=str(struct.unpack('B', self.binary[i+14:i+15])[0]))
            i += 15
        return SPAS



class s101_FASC:
    # A tester sur une ENC qui aura des FASC complets
    
    ''' Lecture du champ Feature Association (FASC)

    :param binary: suite d'octets correspondant au champ FASC
    '''
    def __init__(self, binary):
        self.acronym = 'FASC'
        self.label = 'Feature Association'
        self.binary = binary

    def decode(self, parent):
        ''' Décode le champ FASC

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        FASC = ET.SubElement(parent, 'FASC', NameKey=str(int.from_bytes(self.binary[0:5], byteorder=sys.byteorder, signed=False)),
                                             RRNM=str(struct.unpack('B', self.binary[0:1])[0]),
                                             RRID=str(struct.unpack('I', self.binary[1:5])[0]),
                                             NFAC=str(struct.unpack('h', self.binary[5:7])[0]),
                                             NARC=str(struct.unpack('h', self.binary[7:9])[0]),
                                             FAUI=str(struct.unpack('B', self.binary[9:10])[0]))
        #FA = ET.SubElement(FASC, "FeatureAssociations")
        i = 10
        #print(self.binary[i:])
        while ord(self.binary[i:i+1]) != ENDRECORD:
            j = i
            NATC = str(struct.unpack('h', self.binary[j:j+2])[0])
            ATIX = str(struct.unpack('h', self.binary[j+2:j+4])[0])
            PAIX = str(struct.unpack('h', self.binary[j+4:j+6])[0])
            ATIN = str(struct.unpack('B', self.binary[j+6:j+7])[0])
            j += 7
            k = j
            while ord(self.binary[k:k+1]) != ENDFIELD:
                k += 1
            ET.SubElement(FASC, 'Association', NATC=NATC,
                                               ATIX=ATIX,
                                               PAIX=PAIX,
                                               ATIN=ATIN,
                                               ATVL="".join( chr(x) for x in self.binary[j:k]))
            i = k + 1
        return FASC


class s101_MASK:
    ''' Lecture du champ Masked Spatial Type (MASK)

    :param binary: suite d'octets correspondant au champ MASK
    '''
    def __init__(self, binary):
        self.acronym = 'MASK'
        self.label = 'Masked Spatial Type'
        self.binary = binary

    def decode(self, parent):
        ''' Décode le champ MASK

        :param parent: balise `xml` parent
        :return: structure `xml` avec les valeurs des attributs du champ
        '''
        MASK = ET.SubElement(parent, "MASK")
        i = 0
        while ord(self.binary[i:i+1]) != ENDRECORD:
            ET.SubElement(MASK, 'Mask', NameKey=str(int.from_bytes(self.binary[i:i+5], byteorder=sys.byteorder, signed=False)),
                                        RRNM=str(struct.unpack('B', self.binary[i:i+1])[0]),
                                        RRID=str(struct.unpack('I', self.binary[i+1:i+5])[0]),
                                        MIND=str(struct.unpack('B', self.binary[i+5:i+6])[0]),
                                        MUIN=str(struct.unpack('B', self.binary[i+6:i+7])[0]))
            i += 7
        return MASK



if __name__ == '__main__':
    print("S-101 field")

