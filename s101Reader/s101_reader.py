#!/usr/bin/env python
# -*- coding: UTF-8 -*-

#    Projet          : s101Reader
#    Version         : 1.0
#    Nom du fichier  : s101_reader.py
#    Auteur          : Nicolas GABARRON, SHOM
#    Date            : 13/04/2022
#    Description     : Lecture d'une ENC au format S-101

__author__ = "Nicolas GABARRON, SHOM"
__copyright__ = "Copyright 2022, s101Reader"
__credits__ = "Nicolas GABARRON"
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Nicolas GABARRON"
__email__ = "nicolas.gabarron@shom.fr"
__status__ = "Developpment"



from os.path import basename
import datetime

import xml.etree.cElementTree as ET
import json

from .s101_record import s101Record

from shapely.geometry import (Point, MultiLineString, Polygon)

from s101Catalog.s101_FC import s101_FeatureCatalog

ENDFIELD = 31   # code de fin de champ
ENDRECORD = 30  # code de fin d'enregistrement

DEBUG = False


class s101Reader():
    ''' Lecture d'une ENC S-101

    :param url: chemin de l'ENC
    :param catalog: chemin du Feature Catalog
    '''

    def __init__(self, url, catalog):
        self.url = url

        self.catalog = s101_FeatureCatalog(catalog)

        self.datasetname = basename(self.url).split('.')[0]
        
        # structure xml
        self.root = ET.Element('S101_'+self.datasetname)    # le nom de la balise ne peut pas commencer par un chiffre !!
        self.tree = ET.ElementTree(self.root)
        
        self.root_object = ET.Element('S101_'+self.datasetname)
        self.tree_object = ET.ElementTree(self.root_object)
        
        self.DatasetGeneralInformation = ET.SubElement(self.root, "DatasetGeneralInformation")
        self.DatasetCoordinateReferenceSystem = ET.SubElement(self.root, "DatasetCoordinateReferenceSystem")
        self.InformationRecords = ET.SubElement(self.root, "InformationRecords")
        self.PointRecords = ET.SubElement(self.root, "PointRecords")
        self.MultiPointRecords = ET.SubElement(self.root, "MultiPointRecords")
        self.CurveRecords = ET.SubElement(self.root, "CurveRecords")
        self.CompositeCurveRecords = ET.SubElement(self.root, "CompositeCurveRecords")
        self.SurfaceRecords = ET.SubElement(self.root, "SurfaceRecords")
        self.FeatureTypeRecords = ET.SubElement(self.root, "FeatureTypeRecords")

        # [{field: '', subfields: [{name: '', value:''}, {name: '', value:''}]}]
        self.fieldsDescription = []


    def read(self):
        ''' Lecture des enregistrements de l'ENC

        :return:
        '''

        f = open(self.url, "rb")

        i = 0   # compteur d'enregistrements
        
        nnb = 0
        try:
            while True:
                record_length = int(f.read(5))
                f.seek(-5, 1);
                record = f.read(record_length)
                i += 1
                if i == 1:
                    if DEBUG: print("1er record")
                    r = s101Record(record)
                    if DEBUG: print(r.directory)
                    fields = []
                    for t in r.directory:
                        # [{field: '', subfields: [{name: '', value:''}, {name: '', value:''}]}]
                        if t['tag'] != '0000':
                            field = {'tag': t['tag'], 'subfields': []}
                            vv = r.binary_value2(t)
                            k = 9
                            while ord(vv[k:k+1]) != ENDFIELD:
                                k += 1
                            label = vv[9:k].decode('utf-8')
                            
                            if DEBUG: print(label)
                            
                            j = k + 1
                            k = j
                            while ord(vv[k:k+1]) != ENDFIELD:
                                k += 1
                            subfields = vv[j:k].decode('utf-8')
                            if DEBUG: print(subfields)
                            
                            j = k + 1
                            k = j
                            while ord(vv[k:k+1]) != ENDRECORD:
                                k += 1
                            sf_formats = vv[j:k].decode('utf-8')
                            
                            if DEBUG: print(sf_formats)
                            sf_formats = sf_formats[1:-1]
                            if DEBUG: print(sf_formats)
                            
                            subfields = subfields.replace('*', '')
                            subfields = subfields.replace('\\\\', '!')
                            tab_subfields = subfields.split('!')
                            
                            sff = sf_formats.split(',')
                            
                            #print(vv)
                            coefs = []
                            tab_formats = []
                            for ff in sff:
                                pos = max([ff.find('A'), ff.find('b')])

                                #print(ff, ff[:pos], ff[pos:])
                                if ff[:pos] == '': coefs.append(1)
                                else: coefs.append(int(ff[:pos]))
                                
                                for i in range(0, coefs[-1]):
                                    tab_formats.append(ff[pos:])
                                    
                            i = 0
                            for sf in tab_subfields:
                                field['subfields'].append({'tag': sf, 'format': tab_formats[i]})
                                i += 1

                            fields.append(field)
                            self.fieldsDescription.append(field)
                            
                    
                    if DEBUG: print('fields = {}'.format(fields))

                else:
                    
                    nnb += 1
                    r = s101Record(record)

                    if r.fieldContains('DSID'):
                        rr = r.decode2(self.DatasetGeneralInformation)
                    if r.fieldContains('CSID'):
                        # a terminer
                        rr = r.decode2(self.DatasetCoordinateReferenceSystem)

                    if r.fieldContains('IRID'):
                        IR = ET.SubElement(self.InformationRecords, "Information")
                        rr = r.decode2(IR)

                    if r.fieldContains('PRID'):
                        PR = ET.SubElement(self.PointRecords, "Point")
                        rr = r.decode2(PR)

                    if r.fieldContains('MRID'):
                        MR = ET.SubElement(self.MultiPointRecords, "MultiPoint")
                        rr = r.decode2(MR)

                    if r.fieldContains('CRID'):
                        CT = ET.SubElement(self.CurveRecords, "Curve")
                        rr = r.decode2(CT)

                    if r.fieldContains('CCID'):
                        CC = ET.SubElement(self.CompositeCurveRecords, "CompositeCurve")
                        rr = r.decode2(CC)

                    if r.fieldContains('SRID'):
                        SR = ET.SubElement(self.SurfaceRecords, "Surface")
                        rr = r.decode2(SR)
                            
                    if r.fieldContains('FRID'):
                        FT = ET.SubElement(self.FeatureTypeRecords, "FeatureType")
                        rr = r.decode2(FT)


        except:
            # Your error handling here
            # Nothing for this example
            #print(i)
            #print("Unexpected error:", sys.exc_info()[1])
            #print("fin de fichier")
            pass
        finally:
            f.close()

            # mise à jour des coordonnées
            DSSI = self.root.find("./DatasetGeneralInformation/DSID/DSSI")
            self.updateCoordinate(int(DSSI.get('CMFX')), int(DSSI.get('CMFY')), int(DSSI.get('CMFZ')))

            self.updateAttribute()
            self.updateFeature()
            self.updateInformation()



    #########################################################################
    #
    #  Fonctions de validation de l'ENC
    #
    #########################################################################
    
    def validate(self):
        ''' Validation des valeurs/formats des champs

        :return: {'status': status, 'messages': msg}
            status = True/False
            msg = liste des messages d'erreur
        '''

        status = True
        msg = []
        
        
        ## Vérification des cardinalités des champs
        
        # Dataset General Information
        l = self.root.findall('./DatasetGeneralInformation/DSID')
        if len(l) != 1:
            msg.append('{} : Le champ {} est obligatoire et doit être unique ({} valeurs trouvées)'.format('Dataset General Information', 'DSID', len(l)))
        l = self.root.findall('./DatasetGeneralInformation/DSID/DSSI')
        if len(l) != 1:
            msg.append('{} : Le champ {} est obligatoire et doit être unique ({} valeurs trouvées)'.format('Dataset General Information/DSID', 'DSSI', len(l)))
        
        for c in ['ATCS', 'ITCS', 'FTCS', 'IACS', 'FACS', 'ARCS']:    
            l = self.root.findall('./DatasetGeneralInformation/DSID/{}'.format(c))
            if len(l) > 1:
                msg.append('{} : Le champ {} a une cardinalité <0..1> ({} valeurs trouvées)'.format('Dataset General Information', c, len(l)))
        
        # Dataset Coordinate Reference System
        l = self.root.findall('./DatasetCoordinateReferenceSystem/CSID')
        if len(l) != 1:
            msg.append('{} : Le champ CSID est obligatoire et doit être unique ({} valeurs trouvées)'.format('Dataset Coordinate Reference System', len(l)))
        l = self.root.findall('./DatasetCoordinateReferenceSystem/CSID/CRSH')
        if len(l) == 0:
            msg.append('{} : Le champ CRSH est obligatoire (cardinalité <1..*> - {} valeurs trouvées)'.format('Dataset Coordinate Reference System', len(l)))
        for c in ['CSAX', 'VDAT']:
            l = self.root.findall('./DatasetCoordinateReferenceSystem/CSID/CRSH/{}'.format(c))
            if len(l) > 1:
                msg.append('{} : Le champ {} a une cardinalité <0..1> ({} valeurs trouvées)'.format('Dataset Coordinate Reference System', c, len(l)))
                
        # Information
        l = self.root.findall('./InformationRecords/Information')
        for inf in l:
            nb = len(inf.findall('./IRID'))
            if nb != 1:
                msg.append('{} {}: Le champ IRID est obligatoire et doit être unique ({} valeurs trouvées)'.format('Information', inf.get('NameKey'), nb))
        
        # Point
        l = self.root.findall('./PointRecords/Point')
        for pt in l:
            nb = len(pt.findall('./PRID'))
            if nb != 1:
                msg.append('{} {}: Le champ PRID est obligatoire et doit être unique ({} valeurs trouvées)'.format('Point', pt.get('NameKey'), nb))
            nb_c2it = len(pt.findall('./PRID/C2IT'))
            nb_c3it = len(pt.findall('./PRID/C3IT'))
            if not ((nb_c2it == 0 and nb_c3it == 1) or (nb_c2it == 1 and nb_c3it == 0)):
                msg.append('{} {}: Erreur cardinalité C2IT et C3IT ({} C2IT et {} C3IT trouvés)'.format('Point', pt.get('NameKey'), nb_c2it, nb_c3it))
                
                
        # MultiPoint
        l = self.root.findall('./MultiPointRecords/MultiPoint')
        for mp in l:
            nb = len(mp.findall('./MRID'))
            if nb != 1:
                msg.append('{} {}: Le champ MRID est obligatoire et doit être unique ({} valeurs trouvées)'.format('MultiPoint', mp.get('NameKey'), nb))
            nb_c2il = len(mp.findall('./MRID/C2IL'))
            nb_c3il = len(mp.findall('./MRID/C3IL'))
            if ((nb_c2il == 0 and nb_c3il == 0) or (nb_c2il > 0 and nb_c3il > 0)):
                msg.append('{} {}: Erreur cardinalité C2IL et C3IL ({} C2IL et {} C3IL trouvés)'.format('MultiPoint/MRID', pt.get('NameKey'), nb_c2il, nb_c3il))
        
        # Curve
        l = self.root.findall('./CurveRecords/Curve')
        for cr in l:
            nb = len(cr.findall('./CRID'))
            if nb != 1:
                msg.append('{} {}: Le champ CRID est obligatoire et doit être unique ({} valeurs trouvées)'.format('Curve', mp.get('NameKey'), nb))
            for c in ['PTAS', 'SEGH']: 
                nb = len(cr.findall('./CRID/{}'.format(c)))
                if nb != 1:
                    msg.append('{} {}: Le champ {} est obligatoire et doit être unique ({} valeurs trouvées)'.format('Curve/CRID', cr.get('NameKey'), c, nb))
            nb = len(cr.findall('./CRID/SEGH/C2IL'))
            if nb == 0:
                msg.append('{} {}: Le champ {} est obligatoire ({} valeurs trouvées)'.format('Curve', cr.get('NameKey'), c, nb))
                    
        # CompositeCurve
        l = self.root.findall('./CompositeCurveRecords/CompositeCurve')
        for cc in l:
            nb = len(cc.findall('./CCID'))
            if nb != 1:
                msg.append('{} {}: Le champ CCID est obligatoire et doit être unique ({} valeurs trouvées)'.format('CompositeCurve', cc.get('NameKey'), nb))
        
        # Surface
        l = self.root.findall('./SurfaceRecords/Surface')
        for su in l:
            nb = len(su.findall('./SRID'))
            if nb != 1:
                msg.append('{} {}: Le champ SRID est obligatoire et doit être unique ({} valeurs trouvées)'.format('Surface', su.get('NameKey'), nb))
        
            nb = len(su.findall('./SRID/RIAS'))
            if nb == 0:
                msg.append('{} {}: Le champ RIAS est obligatoire ({} valeurs trouvées)'.format('Surface', cr.get('NameKey'), nb))
                
        # FeatureType
        l = self.root.findall("./FeatureTypeRecords/FeatureType")
        for ft in l:
            nb = len(ft.findall('./FRID'))
            if nb != 1:
                msg.append('{} {}: Le champ {} est obligatoire et doit être unique ({} valeurs trouvées)'.format('FeatureType', ft.get('NameKey'), 'FRID', nb))
            nb = len(ft.findall('./FRID/FOID'))
            if nb != 1:
                msg.append('{} {}: Le champ {} est obligatoire et doit être unique ({} valeurs trouvées)'.format('FeatureType/FRID', ft.get('NameKey'), 'FOID', nb))
        
        
        ## Vérification des valeurs des champs
        
        # Dataset General Information
        DSID = self.root.find("./DatasetGeneralInformation/DSID")
        
        v = self.validateSubfield('DSID', 'RCNM', DSID.get('RCNM'), ['10'])
        if v['status'] == False: msg.append(v['message'])
        v = self.validateSubfield('DSID', 'RCID', DSID.get('RCID'), ['1'])
        if v['status'] == False: msg.append(v['message'])
        v = self.validateSubfield('DSID', 'ENSP', DSID.get('ENSP'), ['S-100 Part 10a'])
        if v['status'] == False: msg.append(v['message'])
        v = self.validateSubfield('DSID', 'ENED', DSID.get('ENED'), ['1.1'])
        if v['status'] == False: msg.append(v['message'])
        v = self.validateSubfield('DSID', 'PRSP', DSID.get('PRSP'), ['INT.IHO.S-101.1.0'])
        if v['status'] == False: msg.append(v['message'])
        v = self.validateSubfield('DSID', 'PRED', DSID.get('PRED'), ['1.0'])
        if v['status'] == False: msg.append(v['message'])
        v = self.validateSubfield('DSID', 'PROF', DSID.get('PROF'), ['1'])
        if v['status'] == False: msg.append(v['message'])
        
        DSNM = DSID.get('DSNM')
        if DSNM[len(DSNM)-4:] != '.000':
            msg.append('{} - {} : Extension non valide ({} - valeur attendue : .000)'.format('DSID', 'DSNM', DSNM[len(DSNM)-4:]))
        if DSNM.find('/') != -1 or DSNM.find('\\') != -1:
            msg.append('{} - {} : Eléments de chemin de fichier détectés'.format('DSID', 'DSNM'))
        
        DSRD = DSID.get('DSRD')
        if len(DSRD) != 8:
            msg.append('{} - {} : Date non valide ({} - format attendu : YYYYMMDD'.format('DSID', 'DSRD', DSRD))
        else:
            correctDate = None
            try:
                newDate = datetime.datetime(int(DSRD[:4]), int(DSRD[4:6]), int(DSRD[6:]))
                correctDate = True
            except ValueError:
                correctDate = False
            if correctDate == False:
                msg.append('{} - {} : Date non valide ({} - format attendu : YYYYMMDD'.format('DSID', 'DSRD', DSRD))
                
        
        v = self.validateSubfield('DSID', 'DSLG', DSID.get('DSLG'), ['EN'])
        if v['status'] == False: msg.append(v['message'])

        DSTC = DSID.findall('./DSTC/Categorie')
        if len(DSTC) != 2: 
            msg.append('{} - {} : {} valeurs trouvées (2 valeurs attendues)'.format('DSID', 'DSTC', len(DSTC)))        
        values = [r.text for r in DSID.findall('./DSTC/Categorie')]
        for v in ['14', '18']:
            if v not in values: msg.append('{} - {} : {} - valeur {} non trouvée dans la liste'.format('DSID', 'DSTC', values, v))


        DSSI = self.root.find("./DatasetGeneralInformation/DSID/DSSI")
        v = self.validateSubfield('DSSI', 'DCOX', DSSI.get('DCOX'), ['0.0'])
        if v['status'] == False: msg.append(v['message'])
        v = self.validateSubfield('DSSI', 'DCOY', DSSI.get('DCOY'), ['0.0'])
        if v['status'] == False: msg.append(v['message'])
        v = self.validateSubfield('DSSI', 'DCOZ', DSSI.get('DCOZ'), ['0.0'])
        if v['status'] == False: msg.append(v['message'])
        v = self.validateSubfield('DSSI', 'CMFX', DSSI.get('CMFX'), ['10000000'])
        if v['status'] == False: msg.append(v['message'])
        v = self.validateSubfield('DSSI', 'CMFY', DSSI.get('CMFY'), ['10000000'])
        if v['status'] == False: msg.append(v['message'])
        v = self.validateSubfield('DSSI', 'CMFZ', DSSI.get('CMFZ'), ['100'])
        if v['status'] == False: msg.append(v['message'])

        
        verif = [['Information', 'NOIR'], ['Point', 'NOPN'], ['MultiPoint', 'NOMN'], 
                 ['Curve', 'NOCN'], ['CompositeCurve', 'NOXN'], ['Surface', 'NOSN'],
                 ['FeatureType', 'NOFR']]
        for v in verif:
            nb = len(self.root.findall("./{}Records/{}".format(v[0], v[0])))
            if int(DSSI.get(v[1])) != nb:
                msg.append('{} - {} : Incohérence avec le nombre d\'enregistrements de type {} ({} vs {})'.format('DSSI', v[1], v[0], DSSI.get(v[1]), nb))

        # ATCS, ITCS, FTCS, IACS, FACS, ARCS
        # vérifier qu'il n'y a pas de doublons dans les Numeric Code
        verif = [['ATCS', 'ANCD'], ['ITCS', 'ITNC'], ['FTCS', 'FTNC'], 
                 ['IACS', 'IANC'], ['FACS', 'FANC'], ['ARCS', 'ARNC']]
        for v in verif:
            ll = []
            for c in self.root.findall('./DatasetGeneralInformation/{}/Code'.format(v[0])):
                ll.append(c.get(v[1]))
            duplicates = self.list_duplicates(ll)
            if len(duplicates) > 0:
                msg.append('{} - {} : Doublons détectés ({})'.format(v[0], v[1], duplicates))
        
        
        # Dataset Coordinate Reference System
        CSID = self.root.find('./DatasetCoordinateReferenceSystem/CSID')
        #ET.dump(CSID)
        v = self.validateSubfield('CSID', 'RCNM', CSID.get('RCNM'), ['15'])
        if v['status'] == False: msg.append(v['message'])
        v = self.validateSubfield('CSID', 'RCID', CSID.get('RCID'), ['1'])
        if v['status'] == False: msg.append(v['message'])
        
        NCRC = CSID.get('NCRC')
        if NCRC in ['', None] or int(NCRC) <= 0:
            msg.append('{} - {} : La valeur doit être supérieure ou égale à 1 (valeur trouvée : {})'.format('CSID', 'NCRC', NCRC))
        else:
            if NCRC == '1':
                C3IT = self.root.findall('.//C3IT')
                C3IL = self.root.findall('.//C3IL')
                if len(C3IT) > 0 or len(C3IL) > 0:
                    msg.append('{} - {} : NCRC = 1 et il y a des champs C3IT et/ou C3IL dans le dataset ({} C3IT, {} C3IL)'.format('CSID', 'NCRC', len(C3IT), len(C3IL)))
        
        
        CRSH = CSID.findall('./CRSH')
        for c in CRSH:
            if c.get('CRST') not in ['1', '5']: msg.append('{} - {} : valeur {} trouvée (valeurs attendues = [1, 5])'.format('CRSH', 'CRST', c.get('CRST')))
            if c.get('CSTY') not in ['1', '3']: msg.append('{} - {} : valeur {} trouvée (valeurs attendues = [1, 3])'.format('CRSH', 'CSTY', c.get('CSTY')))
            if int(c.get('CRIX')) == 1: # horizontal
                if c.get('CRNM') != 'WGS84':
                    msg.append('{} - {} : Pour les CRS horizontal, CRNM doit être égal à \'WGS84\' (valeur {} trouvée)'.format('CRSH', 'CRNM', c.get('CRNM')))
                if c.get('CRSI') != '4326':
                    msg.append('{} - {} : Pour les CRS horizontal, CRSI doit être égal à 4326 (valeur {} trouvée)'.format('CRSH', 'CRSI', c.get('CRSI')))
                if c.get('CRSS') != '2':
                    msg.append('{} - {} : Pour les CRS horizontal, CRSS doit être égal à 2 (valeur {} trouvée)'.format('CRSH', 'CRSS', c.get('CRSS')))
            if int(c.get('CRIX')) > 1:  # vertical
                if c.get('CRNM')[:8] != 'Depth - ':
                    msg.append('{} - {} : Pour les CRS vertical, CRNM doit commencer par \'Depth - \' (valeur {} trouvée)'.format('CRSH', 'CRNM', c.get('CRNM')))
                if c.get('CRSI') != '':
                    msg.append('{} - {} : Pour les CRS vertical, CRSI doit être égal à \'\' (valeur {} trouvée)'.format('CRSH', 'CRSI', c.get('CRSI')))
                if c.get('CRSS') != '255':
                    msg.append('{} - {} : Pour les CRS vertical, CRSS doit être égal à 255 (valeur {} trouvée)'.format('CRSH', 'CRSS', c.get('CRSS')))
        
                CSAX = CSID.findall('./CRSH/CSAX/Axis')
                for cx in CSAX:
                    v = self.validateSubfield('CSAX', 'AXTY', cx.get('AXTY'), ['12'])
                    if v['status'] == False: msg.append(v['message'])
                    v = self.validateSubfield('CSAX', 'AXUM', cx.get('AXUM'), ['4'])
                    if v['status'] == False: msg.append(v['message'])
        
                VDAT = CSID.findall('./CRSH/VDAT')
                for d in VDAT:
                    v = self.validateSubfield('CSAX', 'AXTY', d.get('DTSR'), ['2'])
                    if v['status'] == False: msg.append(v['message'])
                    
                    name = c.get('CRNM')[8:]
                    if d.get('DTNM') != name:
                        msg.append('{} - {} : Valeur incohérente avec CRSH-CRNM ({})'.format('VDAT', 'DTNM', c.get('CRNM')))
        
        
        # Information
        for info in self.root.findall('./InformationRecords/Information'):
            nk = info.get('NameKey')
            IRID = info.find('./IRID')
            v = self.validateSubfield('IRID', 'RCNM', IRID.get('RCNM'), ['150'])
            if v['status'] == False: msg.append(v['message'])
            
            nc = self.ITCS_NumericCodes()
            if IRID.get('NITC') not in nc:
                msg.append('{} - {} : valeur {} trouvée (valeurs attendues dans {})'.format('IRID', 'NITC', IRID.get('NITC'), nc))
            
            v = self.validateSubfield('IRID', 'RUIN', IRID.get('RUIN'), ['1'])
            if v['status'] == False: msg.append(v['message'])
            
            ATTR = info.findall(".//Attribute")
            for a in ATTR:
                nc = self.ATCS_NumericCodes()
                if a.get('NATC') not in nc:
                    msg.append('{} - {} : valeur {} trouvée (valeurs attendues dans {})'.format('ATTR', 'NATC', a.get('NATC'), nc))
            
                v = self.validateSubfield('ATTR', 'ATIN', a.get('ATIN'), ['1'])
                if v['status'] == False: msg.append(v['message'])
            
            # INAS  -- A faire
            INAS = info.findall("./IRID/INAS")
            for i in INAS:
                v = self.validateSubfield('INAS', 'RRNM', i.get('RRNM'), ['150'])
                if v['status'] == False: msg.append('IRID {} : {}'.format(nk, v['message']))
                
                nc = self.IACS_NumericCodes()
                if a.get('NIAC') not in nc:
                    msg.append('IRID {} : {} - {} : valeur {} trouvée (valeurs attendues dans {})'.format(nk, 'INAS', 'NIAC', a.get('NIAC'), nc))
                    
                nc = self.ARCS_NumericCodes()
                if a.get('NARC') not in nc:
                    msg.append('IRID {} : {} - {} : valeur {} trouvée (valeurs attendues dans {})'.format(nk, 'INAS', 'NARC', a.get('NARC'), nc))
                
                v = self.validateSubfield('INAS', 'IUIN', i.get('IUIN'), ['1', '2', '3'])
                if v['status'] == False: msg.append('IRID {} : {}'.format(nk, v['message']))
        
        
        # Point
        for p in self.root.findall('./PointRecords/Point'):
            nk = p.get('NameKey')
            PRID = p.find('./PRID')
            v = self.validateSubfield('PRID', 'RCNM', PRID.get('RCNM'), ['110'])
            if v['status'] == False: msg.append('Point {} : {}'.format(nk, v['message']))
            v = self.validateSubfield('PRID', 'RUIN', PRID.get('RUIN'), ['1'])
            if v['status'] == False: msg.append('Point {} : {}'.format(nk, v['message']))
        
            # C3IT -- A faire
        
        # Multi Point
        for mp in self.root.findall('./MultiPointRecords/MultiPoint'):
            nk = mp.get('NameKey')
            MRID = mp.find('./MRID')
            v = self.validateSubfield('MRID', 'RCNM', MRID.get('RCNM'), ['115'])
            if v['status'] == False: msg.append('MultiPoint {} : {}'.format(nk, v['message']))
            v = self.validateSubfield('MRID', 'RUIN', MRID.get('RUIN'), ['1'])
            if v['status'] == False: msg.append('MultiPoint {} : {}'.format(nk, v['message']))
        
            # INAS -- A faire

            C3IL = mp.findall('./MRID/C3IL')
            for c in C3IL: 
                CRSH = self.root.find("./DatasetCoordinateReferenceSystem/CSID/CRSH[@CRIX=\'{}\']".format(c.get('VCID')))
                if CRSH == None:
                    msg.append('MultiPoint {} : C3IL>VCID ne correspond pas à l\'ID du datum vertical'.format(nk))

        
        # Curve
        for curv in self.root.findall('./CurveRecords/Curve'):
            nk = curv.get('NameKey')
            CRID = curv.find('./CRID')
            v = self.validateSubfield('CRID', 'RCNM', CRID.get('RCNM'), ['120'])
            if v['status'] == False: msg.append('Curve {} : {}'.format(nk, v['message']))
            v = self.validateSubfield('CRID', 'RUIN', CRID.get('RUIN'), ['1'])
            if v['status'] == False: msg.append('Curve {} : {}'.format(nk, v['message']))

            # INAS -- A faire
            
            PTAS = curv.findall('./CRID/PTAS/PointAssociation')
            for pa in PTAS:
                v = self.validateSubfield('PTAS', 'TOPI', pa.get('TOPI'), ['1', '2', '3'])
                if v['status'] == False: msg.append('Curve {} : {}'.format(nk, v['message']))
                
            SEGH = curv.find('./CRID/SEGH')
            v = self.validateSubfield('SEGH', 'INTP', SEGH.get('INTP'), ['4', None])
            if v['status'] == False: msg.append('Curve {} : {}'.format(nk, v['message']))
        
        
        # Composite Curve
        for ccurv in self.root.findall('./CompositeCurveRecords/CompositeCurve'):
            nk = ccurv.get('NameKey')
            CCID = ccurv.find('./CCID')
            v = self.validateSubfield('CCID', 'RCNM', CCID.get('RCNM'), ['125'])
            if v['status'] == False: msg.append('Composite Curve {} : {}'.format(nk, v['message']))
            v = self.validateSubfield('CCID', 'RUIN', CCID.get('RUIN'), ['1'])
            if v['status'] == False: msg.append('Composite Curve {} : {}'.format(nk, v['message']))
            
            # INAS -- A faire
            
            CUCO = ccurv.findall('./CCID/CUCO/Component')
            for cc in CUCO:
                v = self.validateSubfield('CUCO', 'ORNT', cc.get('ORNT'), ['1', '2', None])
                if v['status'] == False: msg.append('Composite Curve {} : {}'.format(nk, v['message']))
            
            
        # Surface
        for surf in self.root.findall('./SurfaceRecords/Surface'):
            nk = surf.get('NameKey')
            SRID = surf.find("./SRID")
            v = self.validateSubfield('SRID', 'RCNM', SRID.get('RCNM'), ['130'])
            if v['status'] == False: msg.append('Surface {} : {}'.format(nk, v['message']))
            v = self.validateSubfield('SRID', 'RUIN', SRID.get('RUIN'), ['1'])
            if v['status'] == False: msg.append('Surface {} : {}'.format(nk, v['message']))
        
            # INAS -- A faire
            
            RIAS = surf.findall('./SRID/RIAS/Ring')
            for r in RIAS:
                v = self.validateSubfield('RIAS', 'ORNT', r.get('ORNT'), ['1', '2', None])
                if v['status'] == False: msg.append('Surface {} : {}'.format(nk, v['message']))
                v = self.validateSubfield('RIAS', 'USAG', r.get('USAG'), ['1', '2', None])
                if v['status'] == False: msg.append('Surface {} : {}'.format(nk, v['message']))
                v = self.validateSubfield('RIAS', 'RAUI', r.get('RAUI'), ['1', None])
                if v['status'] == False: msg.append('Surface {} : {}'.format(nk, v['message']))
            
        
        # Feature Type
        for ft in self.root.findall('./FeatureTypeRecords/FeatureType'):
            nk = ft.get('NameKey')
            FRID = ft.find("./FRID")
            v = self.validateSubfield('FRID', 'RCNM', FRID.get('RCNM'), ['100'])
            if v['status'] == False: msg.append('FeatureType {} : {}'.format(nk, v['message']))
                        
            nc = self.FTCS_NumericCodes()
            if FRID.get('NFTC') not in nc:
                msg.append('{} - {} : valeur {} trouvée (valeurs attendues dans {})'.format('FRID', 'NFTC', FRID.get('NFTC'), nc))
                
            v = self.validateSubfield('FRID', 'RUIN', FRID.get('RUIN'), ['1'])
            if v['status'] == False: msg.append('FeatureType {} : {}'.format(nk, v['message']))
            
            # INAS -- A faire
            
            SPAS = ft.findall('./FRID/SPAS/Association')
            for s in SPAS:
                v = self.validateSubfield('SPAS', 'ORNT', r.get('ORNT'), ['1', '2', '255', None])
                if v['status'] == False: msg.append('FeatureType {} : {}'.format(nk, v['message']))
                v = self.validateSubfield('SPAS', 'SAUI', r.get('SAUI'), ['1', None])
                if v['status'] == False: msg.append('FeatureType {} : {}'.format(nk, v['message']))
            
            # FASC -- A faire    
            
            MASK = ft.findall('./FRID/MASK/Mask')
            for msk in MASK:
                v = self.validateSubfield('MASK', 'MIND', msk.get('MIND'), ['1', '2'])
                if v['status'] == False: msg.append('FeatureType {} : {}'.format(nk, v['message']))
                v = self.validateSubfield('MASK', 'MUIN', msk.get('MUIN'), ['1'])
                if v['status'] == False: msg.append('FeatureType {} : {}'.format(nk, v['message']))
        
        
        if len(msg) > 1: status = False

        return {'status': status, 'messages': msg}
    
    
    def validateSubfield(self, field, subfield, value, expected):
        '''

        :param field:
        :param subfield:
        :param value: valeur du champ
        :param expected: liste des valeurs possibles du champ
        :return:
        '''
        status = True
        msg = ''
        if value not in expected:
            status = False
            msg = '{} - {} : valeur {} trouvée (valeur attendue dans {})'.format(field, subfield, value, expected)
            
        return {'status': status, 'message': msg}


    def list_duplicates(self, seq):
        seen = set()
        seen_add = seen.add
        seen_twice = set( x for x in seq if x in seen or seen_add(x) )
        return list( seen_twice )
    
    
    #########################################################################
    #
    #  Fonctions d'accès aux champs de métadonnées et paramètres de l'ENC
    #
    #########################################################################
    
    def ATCS_Codes(self):
        ''' Renvoie la liste des labels des Attribute Codes

        :return: tableau contenant les labels
        '''
        codes = []
        ATCS = self.root.find('./DatasetGeneralInformation/DSID/ATCS')
        for c in ATCS.findall('./Code'): 
            codes.append(c.get('ATCD'))
        return codes

    def ATCS_NumericCodes(self):
        ''' Renvoie la liste des codes numériques des Attribute Codes

        :return: tableau contenant les codes
        '''
        codes = []
        ATCS = self.root.find('./DatasetGeneralInformation/DSID/ATCS')
        for c in ATCS.findall('./Code'): 
            codes.append(c.get('ANCD'))
        return codes
    
    def ITCS_Codes(self):
        ''' Renvoie la liste des labels des Information Type Codes

        :return: tableau contenant les labels
        '''
        codes = []
        ITCS = self.root.find('./DatasetGeneralInformation/DSID/ITCS')
        for c in ITCS.findall('./Code'): 
            codes.append(c.get('ITCD'))
        return codes

    def ITCS_NumericCodes(self):
        ''' Renvoie la liste des codes numériques des Information Type Codes

        :return: tableau contenant les codes
        '''
        codes = []
        ITCS = self.root.find('./DatasetGeneralInformation/DSID/ITCS')
        for c in ITCS.findall('./Code'): 
            codes.append(c.get('ITNC'))
        return codes
    
    def FTCS_Codes(self):
        ''' Renvoie la liste des labels des Feature Type Codes

        :return: tableau contenant les labels
        '''
        codes = []
        FTCS = self.root.find('./DatasetGeneralInformation/DSID/FTCS')
        for c in FTCS.findall('./Code'): 
            codes.append(c.get('FTCD'))
        return codes

    def FTCS_NumericCodes(self):
        ''' Renvoie la liste des codes numériques des Feature Type Codes

        :return: tableau contenant les codes
        '''
        codes = []
        FTCS = self.root.find('./DatasetGeneralInformation/DSID/FTCS')
        for c in FTCS.findall('./Code'): 
            codes.append(c.get('FTNC'))
        return codes
    
    def IACS_Codes(self):
        ''' Renvoie la liste des labels des Information Association Codes

        :return: tableau contenant les labels
        '''
        codes = []
        IACS = self.root.find('./DatasetGeneralInformation/DSID/IACS')
        for c in IACS.findall('./Code'): 
            codes.append(c.get('IACD'))
        return codes

    def IACS_NumericCodes(self):
        ''' Renvoie la liste des codes numériques des Information Association Codes

        :return: tableau contenant les codes
        '''
        codes = []
        IACS = self.root.find('./DatasetGeneralInformation/DSID/IACS')
        for c in IACS.findall('./Code'): 
            codes.append(c.get('IANC'))
        return codes
    
    def FACS_Codes(self):
        ''' Renvoie la liste des labels des Feature Association Codes

        :return: tableau contenant les labels
        '''
        codes = []
        FACS = self.root.find('./DatasetGeneralInformation/DSID/FACS')
        for c in FACS.findall('./Code'): 
            codes.append(c.get('FACD'))
        return codes

    def FACS_NumericCodes(self):
        ''' Renvoie la liste des codes numériques des Feature Association Codes

        :return: tableau contenant les codes
        '''
        codes = []
        FACS = self.root.find('./DatasetGeneralInformation/DSID/FACS')
        for c in FACS.findall('./Code'): 
            codes.append(c.get('FANC'))
        return codes
    
    def ARCS_Codes(self):
        ''' Renvoie la liste des labels des Association Role Codes

        :return: tableau contenant les labels
        '''
        codes = []
        ARCS = self.root.find('./DatasetGeneralInformation/DSID/ARCS')
        for c in ARCS.findall('./Code'): 
            codes.append(c.get('ARCD'))
        return codes

    def ARCS_NumericCodes(self):
        ''' Renvoie la liste des codes numériques des Association Role Codes

        :return: tableau contenant les codes
        '''
        codes = []
        ARCS = self.root.find('./DatasetGeneralInformation/DSID/ARCS')
        for c in ARCS.findall('./Code'): 
            codes.append(c.get('ARNC'))
        return codes


    #########################################################################
    #
    #  Fonctions de conversion en objets
    #
    #########################################################################
    
    def toObjects(self):
        ''' Conversion en objets (association de la géométrie aux features)
            Mise à jour de l'élément root_object
        '''
        ft = self.FeatureTypeRecords.findall('./FeatureType')
        
        i = 0
        for f in ft:
            i += 1
            obj = ET.SubElement(self.root_object, "Object")
            feature = ET.SubElement(obj, "Feature")
            geometry = ET.SubElement(obj, "Geometry")

            feature.insert(0, f)

            spas = f.findall('./FRID/SPAS/Association')
            for s in spas:
                nk = s.get('NameKey')

                """
                geom_pt = self.PointRecords.findall("./Point[@NameKey='"+nk+"']")
                geom_mp = self.MultiPointRecords.findall("./MultiPoint[@NameKey='"+nk+"']")
                geom_c = self.CurveRecords.findall("./Curve[@NameKey='"+nk+"']")
                geom_cc = self.CompositeCurveRecords.findall("./CompositeCurve[@NameKey='"+nk+"']")
                geom_s = self.SurfaceRecords.findall("./Surface[@NameKey='"+nk+"']")
                """

                geom_pt = self.PointRecords.findall(f"./Point[@NameKey='{nk}']")
                geom_mp = self.MultiPointRecords.findall(f"./MultiPoint[@NameKey='{nk}']")
                geom_c = self.CurveRecords.findall(f"./Curve[@NameKey='{nk}']")
                geom_cc = self.CompositeCurveRecords.findall(f"./CompositeCurve[@NameKey='{nk}']")
                geom_s = self.SurfaceRecords.findall(f"./Surface[@NameKey='{nk}']")

                for g in geom_pt: geometry.insert(0, g)
                for g in geom_mp: geometry.insert(0, g)
                for g in geom_c: geometry.insert(0, g)
                for g in geom_cc: geometry.insert(0, g)
                for g in geom_s: geometry.insert(0, g)
                

    #########################################################################
    #
    #  Fonctions d'export
    #
    #########################################################################
    
    def geometry2geojson(self, geom):
        
        geojson = {'type': '', 'coordinates':[]}
                
        if geom.tag == 'Point':
            geojson['type'] = 'Point'
            cc = geom.find('./PRID/C2IT')
            if cc is not None:
                geojson['coordinates'] = [float(cc.get('XCOO')), float(cc.get('YCOO'))]
            else:
                cc = geom.find('./PRID/C3IT')
                geojson['coordinates'] = [float(cc.get('XCOO')), float(cc.get('YCOO')), float(cc.get('ZCOO'))]
        elif geom.tag == 'CompositeCurve': 
            geojson['type'] = 'MultiLineString'
            for co in geom.findall('./CCID/CUCO/Component'):
                nk = co.get('NameKey')
                geom_c = self.root.findall("./CurveRecords/Curve[@NameKey='"+nk+"']/CRID")
                for g in geom_c:
                    pts = []
                    if co.get('ORNT') == '1':
                        for pt in g.findall('./SEGH/C2IL/Point'):
                            pts.append([float(pt.get('XCOO')), float(pt.get('YCOO'))])
                    else:
                        for pt in reversed(g.findall('./SEGH/C2IL/Point')):
                            pts.append([float(pt.get('XCOO')), float(pt.get('YCOO'))])
                    geojson['coordinates'].append(pts)
        elif geom.tag == 'Surface':
            geojson['type'] = 'MultiPolygon'
            surf = []
            for ring in geom.findall('./SRID/RIAS/Ring'):
                pts_ring = []
                nk_ring = ring.get('NameKey')
                pts_cc = []
                for cc in self.root.findall("./CompositeCurveRecords/CompositeCurve[@NameKey='"+nk_ring+"']/CCID"):
                    pts_component = []
                    for co in cc.findall('./CUCO/Component'):
                        nk_comp = co.get('NameKey')
                        geom_c = self.root.findall("./CurveRecords/Curve[@NameKey='"+nk_comp+"']/CRID")
                        for g in geom_c:
                            pts = []
                            if co.get('ORNT') == '1':
                                for pt in g.findall('./SEGH/C2IL/Point'):
                                    pts.append([float(pt.get('XCOO')), float(pt.get('YCOO'))])
                            else:
                                for pt in reversed(g.findall('./SEGH/C2IL/Point')):
                                    pts.append([float(pt.get('XCOO')), float(pt.get('YCOO'))])
                            for pt in pts: pts_component.append(pt)
                    for pt in pts_component: pts_cc.append(pt)
                if ring.get('ORNT') == '1':
                    for pt in pts_cc:
                        pts_ring.append(pt)
                else:
                    for pt in reversed(pts_cc):
                        pts_ring.append(pt)
                surf.append(pts_ring)
            geojson['coordinates'].append(surf)

        return geojson
    
    
    def object2geojson(self, nk, geom):
        '''
        :param nk: NameKey de l'objet
        :param geom: Point, CompositeCurve, Surface
        :return: objet geojson
        '''
        geojson = {'type':'FeatureCollection', 'features':[]}
        
        feature = {'type':'Feature',
                   'properties':{},
                   'geometry':{'type':'',
                               'coordinates':[]}}
        
        for o in self.root_object.findall('./Object'):
            if o.find(f"./Feature/FeatureType[@NameKey='{nk}']") is not None:
                obj = o
                
        #ET.dump(obj)
        
        feature['properties']['NameKey'] = nk
        
        ft = obj.find('./Feature/FeatureType')
        FRID = ft.find('./FRID')
        feature['properties']['code'] = FRID.get('Code')
        attr = ft.find('./FRID/ATTR')
        if attr is not None:
            xml_str = ET.tostring(attr, encoding='unicode')
            xml_str = xml_str.replace('"', '\"', xml_str.count('"'))
            feature['properties']['ATTR'] = xml_str

        FOID = ft.find('./FRID/FOID')
        feature['properties']['FOID'] = '{}{}{}'.format(FOID.get('AGEN'), FOID.get('FIDN'), FOID.get('FIDS'))

        #ET.dump(obj.find('./Geometry/{}'.format(geom)))
        #feature['geometry'] = self.geometry2geojson(obj.find('./Geometry/{}'.format(geom)))
        feature['geometry'] = self.geometry2geojson(obj.find(f'./Geometry/{geom}'))
       
        
        geojson['features'].append(feature)
        #print(geojson)
        return geojson
    
    
    def convert2geojson(self, geom):
        ''' Converti les objets de type geom en geojson

        :param geom: Point, CompositeCurve, Surface
        :return: objet geojson
        '''
        geojson = {'type':'FeatureCollection', 'features':[]}
        
        objects = self.root_object.findall("./Object")

        i = 0
        for obj in objects:
            i += 1
            f = obj.find('./Feature/FeatureType')

            feature = {'type':'Feature',
                       'properties':{},
                       'geometry':{'type':'',
                                   'coordinates':[]}}
            feature['properties']['NameKey'] = f.get('NameKey')
            FRID = f.find('./FRID')
            feature['properties']['code'] = FRID.get('Code')

            attr = f.find('./FRID/ATTR')
            if attr is not None:
                xml_str = ET.tostring(f.find('./FRID/ATTR'), encoding='unicode')
                xml_str = xml_str.replace('"', '\"', xml_str.count('"'))
                feature['properties']['ATTR'] = xml_str

            FOID = f.find('./FRID/FOID')
            #feature['properties']['FOID'] = FOID.get('AGEN')+FOID.get('FIDN')+FOID.get('FIDS')
            feature['properties']['FOID'] = f"{FOID.get('AGEN').rjust(5, '0')}-{FOID.get('FIDN').rjust(10, '0')}-{FOID.get('FIDS').rjust(5, '0')}"

            nb = 0
            if geom == 'Point':
                feature['geometry']['type'] = 'Point'
                for pt in obj.findall('./Geometry/Point'):
                    nb += 1
                    cc = pt.find('./PRID/C2IT')
                    if cc is not None:
                        feature['geometry']['coordinates'] = [float(cc.get('XCOO')), float(cc.get('YCOO'))]
                    else:
                        cc = pt.find('./PRID/C3IT')
                        feature['geometry']['coordinates'] = [float(cc.get('XCOO')), float(cc.get('YCOO')), float(cc.get('ZCOO'))]
            elif geom == 'MultiPoint':
                feature['geometry']['type'] = 'MultiPoint'
                for mp in obj.findall('./Geometry/MultiPoint'):
                    nb += 1
                    for pt in mp.findall('./MRID/C2IL/Point'):
                        feature['geometry']['coordinates'].append([float(pt.get('XCOO')), float(pt.get('YCOO'))])
                    for pt in mp.findall('./MRID/C3IL/Point'):
                        feature['geometry']['coordinates'].append([float(pt.get('XCOO')), float(pt.get('YCOO')), float(pt.get('ZCOO'))])
            elif geom == 'CompositeCurve':
                for cc in obj.findall('./Geometry/CompositeCurve/CCID'):
                    feature['geometry']['type'] = 'MultiLineString'
                    nb += 1
                    for co in cc.findall('./CUCO/Component'):
                        nk = co.get('NameKey')
                        geom_c = self.root.findall("./CurveRecords/Curve[@NameKey='"+nk+"']/CRID")
                        for g in geom_c:
                            pts = []
                            if co.get('ORNT') == '1':
                                for pt in g.findall('./SEGH/C2IL/Point'):
                                    pts.append([float(pt.get('XCOO')), float(pt.get('YCOO'))])
                            else:
                                for pt in reversed(g.findall('./SEGH/C2IL/Point')):
                                    pts.append([float(pt.get('XCOO')), float(pt.get('YCOO'))])
                            feature['geometry']['coordinates'].append(pts)
            elif geom == 'Surface':
                # là ça marche avec les trous dans les polygons !!!!!!!!!!!!!!!!!!!
                for cc in obj.findall('./Geometry/Surface/SRID'):
                    feature['geometry']['type'] = 'MultiPolygon'
                    nb += 1
                    surf = []
                    for ring in cc.findall('./RIAS/Ring'):
                        pts_ring = []
                        nk_ring = ring.get('NameKey')
                        pts_cc = []
                        for cc in self.root.findall("./CompositeCurveRecords/CompositeCurve[@NameKey='"+nk_ring+"']/CCID"):
                            pts_component = []
                            for co in cc.findall('./CUCO/Component'):
                                nk_comp = co.get('NameKey')
                                geom_c = self.root.findall("./CurveRecords/Curve[@NameKey='"+nk_comp+"']/CRID")
                                for g in geom_c:
                                    pts = []
                                    if co.get('ORNT') == '1':
                                        for pt in g.findall('./SEGH/C2IL/Point'):
                                            pts.append([float(pt.get('XCOO')), float(pt.get('YCOO'))])
                                    else:
                                        for pt in reversed(g.findall('./SEGH/C2IL/Point')):
                                            pts.append([float(pt.get('XCOO')), float(pt.get('YCOO'))])
                                    for pt in pts: pts_component.append(pt)
                            for pt in pts_component: pts_cc.append(pt)
                        if ring.get('ORNT') == '1':
                            for pt in pts_cc:
                                pts_ring.append(pt)
                        else:
                            for pt in reversed(pts_cc):
                                pts_ring.append(pt)
                        surf.append(pts_ring)
                    feature['geometry']['coordinates'].append(surf)

            if nb>0: geojson['features'].append(feature)
        
        return geojson
    
    
    def geojson2file(self, geojson, outfile):
        ''' Exporte le geojson dans un fichier (obligé sinon mauvais encodage (' au lieu de ") et impossible de relire le fichier en tant que geojson dans QGIS

        :param geojson: geojson à exporter
        :param outfile: chemin du fichier à créer
        :return:
        '''
        gjson = ''
        gjson += '{'
        gjson += '"type": "FeatureCollection",'
        gjson += '"features": ['
        nb = 0
        for f in geojson['features']:
            nb += 1
            gjson += json.dumps(f)
            if nb < len(geojson['features']):
                gjson += ','
        gjson += ']'
        gjson += '}'
        
        fichier = open(outfile, 'w')
        fichier.write('{}'.format(gjson))
        fichier.close()

        
    def xml2file(self, tree, out):
        ''' Exporte un arbre xml dans un fichier

        :param tree: arbre xml à exporter
        :param out: chemin du fichier à créer
        :return:
        '''
        with open(out, 'wb') as f:
            tree.write(f)


    def geojsonFilterByCode(self, geojson, codes):
        ''' Filtre un geojson suivant l'attribut 'code' (camelCase)

        :param geojson: geojson à filtrer
        :param codes: tableau des valeurs de l'attribut 'code' (camelCase) à filtrer
        :return: objet geojson
        '''
        filtered = {'type':'FeatureCollection', 'features':[]}
        for f in geojson['features']:
            if f['properties']['code'] in codes:
                filtered['features'].append(f)

        return filtered


    #########################################################################
    #
    #  Fonctions d'update des coordonnées, libellé des attributs et 
    #  libéllé des objets
    #
    #########################################################################

    def attributeCode(self, num):
        ''' Renvoie le libellé de l'attribut à partir de son code numérique

        :param num: code numérique de l'attribut
        :return:
        '''
        for a in self.root.findall('./DatasetGeneralInformation/DSID/ATCS/Code'):
            if a.get('ANCD') == num:
                return a.get('ATCD')
        return None


    def featureCode(self, num):
        ''' Renvoie le libellé de l'objet à partir de son code numérique

        :param num: code numérique de l'objet
        :return:
        '''
        for a in self.root.findall('./DatasetGeneralInformation/DSID/FTCS/Code'):
            if a.get('FTNC') == num:
                return a.get('FTCD')
        return None
    
    def informationCode(self, num):
        ''' Renvoie le libellé de l'information à partir de son code numérique

        :param num: code numérque de l'information
        :return:
        '''
        for a in self.root.findall('./DatasetGeneralInformation/DSID/ITCS/Code'):
            if a.get('ITNC') == num:
                return a.get('ITCD')
        return None


    def updateCoordinate(self, cmfx, cmfy, cmfz):
        ''' Mise à jour des coordonnées

        :param cmfx: facteur multiplicatif pour les longitudes
        :param cmfy: facteur multiplicatif pour les latitudes
        :param cmfz: facteur multiplicatif pour l'altitude
        '''
        # Point
        for pt in self.root.findall('./PointRecords/Point/PRID/C2IT'):
            pt.set('YCOO', str(int(pt.get('YCOO'))/cmfy))
            pt.set('XCOO', str(int(pt.get('XCOO'))/cmfx))
        for pt in self.root.findall('./PointRecords/Point/PRID/C3IT'):
            pt.set('YCOO', str(int(pt.get('YCOO'))/cmfy))
            pt.set('XCOO', str(int(pt.get('XCOO'))/cmfx))
            pt.set('ZCOO', str(int(pt.get('ZCOO'))/cmfz))

        # MultiPoint
        for mp in self.root.findall('./MultiPointRecords/MultiPoint/MRID/C2IL/Point'):
            mp.set('YCOO', str(int(mp.get('YCOO'))/cmfy))
            mp.set('XCOO', str(int(mp.get('XCOO'))/cmfx))
        for mp in self.root.findall('./MultiPointRecords/MultiPoint/MRID/C3IL/Point'):
            mp.set('YCOO', str(int(mp.get('YCOO'))/cmfy))
            mp.set('XCOO', str(int(mp.get('XCOO'))/cmfx))
            mp.set('ZCOO', str(int(mp.get('ZCOO'))/cmfz))

        # Curve
        for c in self.root.findall('./CurveRecords/Curve/CRID/SEGH/C2IL/Point'):
            c.set('YCOO', str(int(c.get('YCOO'))/cmfy))
            c.set('XCOO', str(int(c.get('XCOO'))/cmfx))


    def updateAttribute(self):
        ''' Rajoute le code (label) de l'attribut dans toutes les balises `Attribute`
        '''
        for f in self.root.findall(".//Attribute"):
            f.set('Code', self.attributeCode(f.get('NATC')))


    def updateFeature(self):
        ''' Rajoute le code (label) de l'objet dans toutes les balises `FRID`
        '''
        for f in self.root.findall("./FeatureTypeRecords/FeatureType/FRID"):
            f.set('Code', self.featureCode(f.get('NFTC')))
            
    def updateInformation(self):
        ''' Rajoute le code (label) de l'information dans toutes les balises `IRID`
        '''
        for f in self.root.findall("./InformationRecords/Information/IRID"):
            f.set('Code', self.informationCode(f.get('NITC')))



    #########################################################################
    #
    #  Fonctions d'extraction de géométries et objets
    #
    #########################################################################
    
    def pointGeometry(self, nk):
        p = self.PointRecords.find(f"./Point[@NameKey='{nk}']/PRID")
        C2IT = p.find('./C2IT')
        if C2IT is not None:
            res = {'XCOO': float(C2IT.get('XCOO')),
                   'YCOO': float(C2IT.get('YCOO'))}
        else:
            C3IT = p.find('./C3IT')
            res = {'XCOO': float(C3IT.get('XCOO')),
                   'YCOO': float(C3IT.get('YCOO')),
                   'ZCOO': float(C3IT.get('ZCOO'))}
        return res
    
    
    def multipointGeometry(self, nk):
        res = []
        p = self.MultiPointRecords.find(f"./MultiPoint[@NameKey='{nk}']/MRID")
        C2IL = p.find('./C2IL')
        if C2IL is not None:
            for pt in C2IL.findall('./Point'):
                res.append({'XCOO':float(pt.get('XCOO')), 
                            'YCOO':float(pt.get('YCOO'))})
        else:
            C3IL = p.find('./C3IL')
            for pt in C3IL.findall('./Point'):
                res.append({'XCOO': float(pt.get('XCOO')),
                            'YCOO': float(pt.get('YCOO')),
                            'ZCOO': float(pt.get('ZCOO'))})
        return res
    
    
    def compositecurveGeometry(self, nk):
        res = []
        cc = self.CompositeCurveRecords.find(f"./CompositeCurve[@NameKey='{nk}']/CCID")
        for co in cc.findall('./CUCO/Component'):
            pts = []
            cu = self.CurveRecords.find(f"./Curve[@NameKey='{co.get('NameKey')}']/CRID")
            if co.get('ORNT') == '1':
                for pt in cu.findall('./SEGH/C2IL/Point'):
                    pts.append({'XCOO': float(pt.get('XCOO')),
                                'YCOO': float(pt.get('YCOO'))})
            else:
                for pt in reversed(cu.findall('./SEGH/C2IL/Point')):
                    pts.append({'XCOO': float(pt.get('XCOO')),
                                'YCOO': float(pt.get('YCOO'))})
            res.append(pts)
        return res
    
    
    def surfaceGeometry(self, nk):
        res = []
        su = self.SurfaceRecords.find(f"./Surface[@NameKey='{nk}']/SRID")
        """
        # ca marche
        for ring in su.findall('./RIAS/Ring'):
            pts_ring = []
            cc = self.CompositeCurveRecords.find("./CompositeCurve[@NameKey='"+ring.get('NameKey')+"']/CCID")
            for co in cc.findall('./CUCO/Component'):                
                nk_comp = co.get('NameKey')
                geom_c = self.CurveRecords.find("./Curve[@NameKey='"+nk_comp+"']/CRID")
                pts_comp = []
                if co.get('ORNT') == '1':
                    for pt in geom_c.findall('./SEGH/C2IL/Point'):
                        pts_comp.append({'XCOO':float(pt.get('XCOO')), 
                                         'YCOO':float(pt.get('YCOO'))})
                else:
                    for pt in reversed(geom_c.findall('./SEGH/C2IL/Point')):
                        pts_comp.append({'XCOO':float(pt.get('XCOO')), 
                                         'YCOO':float(pt.get('YCOO'))})       
                if ring.get('ORNT') == '1':
                    for pt in pts_comp:
                        pts_ring.append(pt)
                else:
                    for pt in reversed(pts_comp):
                        pts_ring.append(pt)
                
            res.append(pts_ring)
        """
        for ring in su.findall('./RIAS/Ring'):
            pts_ring = []
            nk_ring = ring.get('NameKey')
            pts_cc = []
            for cc in self.CompositeCurveRecords.findall(f"./CompositeCurve[@NameKey='{nk_ring}']/CCID"):
                pts_comp = []
                for co in cc.findall('./CUCO/Component'):                
                    nk_comp = co.get('NameKey')
                    geom_c = self.CurveRecords.findall(f"./Curve[@NameKey='{nk_comp}']/CRID")
                    for g in geom_c:
                        pts = []
                        if co.get('ORNT') == '1':
                            for pt in g.findall('./SEGH/C2IL/Point'):
                                pts.append({'XCOO': float(pt.get('XCOO')),
                                            'YCOO': float(pt.get('YCOO'))})
                        else:
                            for pt in reversed(g.findall('./SEGH/C2IL/Point')):
                                pts.append({'XCOO': float(pt.get('XCOO')),
                                            'YCOO': float(pt.get('YCOO'))})
                        for pt in pts: pts_comp.append(pt)
                for pt in pts_comp: pts_cc.append(pt)
            
            for pt in pts_comp:
                pts_ring.append(pt)
                
            res.append(pts_ring)
        
        return res

        
    def geometry2wkt(self, primitive, geom):
        geometry = ''
        if primitive == 'point':
            if len(geom) == 2: geometry = 'POINT({} {})'.format(geom['XCOO'], geom['YCOO'])
            elif len(geom) == 3: geometry = 'POINT({} {} {})'.format(geom['XCOO'], geom['YCOO'], geom['ZCOO'])
        elif primitive == 'multipoint':
            if len(geom[0]) == 2: dim = 2
            elif len(geom[0]) == 3: dim = 3
            geometry = 'MULTIPOINT('
            for g in geom:
                if dim == 2: geometry += '({} {}),'.format(g['XCOO'], g['YCOO'])
                elif dim == 3: geometry += '({} {} {}),'.format(g['XCOO'], g['YCOO'], g['ZCOO'])
            geometry = geometry[:-1]
            geometry += ')'
        elif primitive == 'compositecurve':
            geometry = 'MULTILINESTRING('
            for g in geom:
                geometry += '('
                for pt in g:
                    geometry += '{} {},'.format(pt['XCOO'], pt['YCOO'])
                geometry = geometry[:-1]
                geometry += '),'
            geometry = geometry[:-1]
            geometry += ')'
        elif primitive == 'surface':
            geometry = 'POLYGON('
            for g in geom:
                geometry += '('
                for pt in g:
                    geometry += '{} {},'.format(pt['XCOO'], pt['YCOO'])
                geometry = geometry[:-1]
                geometry += '),'
            geometry = geometry[:-1]
            geometry += ')'
        
        return geometry
    
    
    def geometry2shapely(self, primitive, geom): 
        #print(geom)
        if primitive == 'point':
            #if len(geom) == 2: geometry = 'POINT({} {})'.format(geom['XCOO'], geom['YCOO'])
            #elif len(geom) == 3: geometry = 'POINT({} {} {})'.format(geom['XCOO'], geom['YCOO'], geom['ZCOO'])
            if len(geom) == 2: geometry = (geom['XCOO'], geom['YCOO'])
            elif len(geom) == 3: geometry = (geom['XCOO'], geom['YCOO'], geom['ZCOO'])
        elif primitive == 'multipoint':
            if len(geom[0]) == 2: dim = 2
            elif len(geom[0]) == 3: dim = 3
            #print('dim', dim)
            geometry = 'MULTIPOINT('
            for g in geom:
                if dim == 2: geometry += '({} {}),'.format(g['XCOO'], g['YCOO'])
                elif dim == 3: geometry += '({} {} {}),'.format(g['XCOO'], g['YCOO'], g['ZCOO'])
            geometry = geometry[:-1]
            geometry += ')'
        elif primitive == 'compositecurve':
            """
            geometry = 'MULTILINESTRING('
            for g in geom:
                geometry += '('
                for pt in g:
                    geometry += '{} {},'.format(pt['XCOO'], pt['YCOO'])
                geometry = geometry[:-1]
                geometry += '),'
            geometry = geometry[:-1]
            geometry += ')'
            """
            geometry = []
            for l in geom:
                line = []
                for pt in l:
                    line.append((pt['XCOO'], pt['YCOO']))
                geometry.append(line)
            
        elif primitive == 'surface':
            geometry = []
            
            exterior = []
            interior = []
            for pt in geom[0]:
                exterior.append((pt['XCOO'], pt['YCOO']))
            if len(geom) > 1:
                for g in geom[1:]:
                    hole = []
                    for pt in g:
                        hole.append((pt['XCOO'], pt['YCOO']))
                    interior.append(hole)
                
            geometry.append(exterior)
            geometry.append(interior)
            
        return geometry


    def geometry2shapely2(self, primitive, nk): 
        if primitive == 'Point':
            geom = self.pointGeometry(nk)
            if len(geom) == 2: geometry = (geom['XCOO'], geom['YCOO'])
            elif len(geom) == 3: geometry = (geom['XCOO'], geom['YCOO'], geom['ZCOO'])
            geometry = Point(geometry)
        elif primitive == 'multipoint':
            if len(geom[0]) == 2: dim = 2
            elif len(geom[0]) == 3: dim = 3
            #print('dim', dim)
            geometry = 'MULTIPOINT('
            for g in geom:
                if dim == 2: geometry += '({} {}),'.format(g['XCOO'], g['YCOO'])
                elif dim == 3: geometry += '({} {} {}),'.format(g['XCOO'], g['YCOO'], g['ZCOO'])
            geometry = geometry[:-1]
            geometry += ')'
        elif primitive == 'CompositeCurve':
            geom = self.compositecurveGeometry(nk)            
            geometry = []
            for l in geom:
                line = []
                for pt in l:
                    line.append((pt['XCOO'], pt['YCOO']))
                geometry.append(line)
            geometry = MultiLineString(geometry)
        elif primitive == 'Surface':
            geom = self.surfaceGeometry(nk)
            geometry = []            
            exterior = []
            interior = []
            for pt in geom[0]:
                exterior.append((pt['XCOO'], pt['YCOO']))
            if len(geom) > 1:
                for g in geom[1:]:
                    hole = []
                    for pt in g:
                        hole.append((pt['XCOO'], pt['YCOO']))
                    interior.append(hole)
                
            geometry.append(exterior)
            geometry.append(interior)
            
            geometry = Polygon(geometry[0], geometry[1])
            
        return geometry
        
        
    def geometry2(self):
        """
        geom = self.compositecurveGeometry('113021')
        print(geom)
        print(self.geometry2wkt('compositecurve', '2D', geom))
        """
        """
        geom = self.surfaceGeometry('64642')
        print(geom)
        print(self.geometry2wkt('surface', '2D', geom))
        """
        
        geom = self.surfaceGeometry('388994')
        print(geom)
        print(self.geometry2wkt('surface', geom))
        
        """
        geom = self.pointGeometry('941166')
        print(geom)
        print(self.geometry2wkt('point', geom))
        """
        """
        geom = self.multipointGeometry('371')
        print(geom)
        print(self.geometry2wkt('multipoint', geom))
        """
        

    def geometry(self, primitive, nk):
        # recompose une géométrie
        pts = []
        if primitive == 'Point':
            p = self.PointRecords.find("./Point[@NameKey='"+nk+"']/PRID")
            C2IT = p.find('./C2IT')
            if C2IT is not None:
                pts.append({'XCOO':float(C2IT.get('XCOO')), 
                            'YCOO':float(C2IT.get('YCOO'))})
            else:
                C3IT = p.find('./C3IT')
                pts.append({'XCOO':float(C3IT.get('XCOO')), 
                            'YCOO':float(C3IT.get('YCOO')), 
                            'ZCOO':float(C3IT.get('ZCOO'))})
        elif primitive == 'MultiPoint':
            mp = self.MultiPointRecords.find("./MultiPoint[@NameKey='"+nk+"']/MRID")
            C2IL = mp.findall('./C2IL')
            pts = []
            if len(C2IL) > 0:
                for pt in mp.findall('./C2IL/Point'):
                    pts.append({'XCOO':float(pt.get('XCOO')), 
                                'YCOO':float(pt.get('YCOO'))})
            else:
                for pt in mp.findall('./C3IL/Point'):
                    pts.append({'XCOO':float(pt.get('XCOO')), 
                                'YCOO':float(pt.get('YCOO')), 
                                'ZCOO':float(pt.get('ZCOO'))})
        elif primitive == 'Curve':
            cu = self.CurveRecords.find("./Curve[@NameKey='"+nk+"']/CRID")
            PTAS = cu.findall('./PTAS/PointAssociation')
            if len(PTAS) == 1:
                nk = PTAS[0].get('NameKey')
                for pt in cu.findall('./SEGH/C2IL/Point'):
                    pts.append({'XCOO':float(pt.get('XCOO')), 
                                'YCOO':float(pt.get('YCOO'))})
            else:
                for pt in cu.findall('./SEGH/C2IL/Point'):
                    pts.append({'XCOO':float(pt.get('XCOO')), 
                                'YCOO':float(pt.get('YCOO'))})
        elif primitive == 'CompositeCurve':
            cc = self.CompositeCurveRecords.find("./CompositeCurve[@NameKey='"+nk+"']/CCID")
            for co in cc.findall('./CUCO/Component'):
                cu = self.CurveRecords.find("./Curve[@NameKey='"+co.get('NameKey')+"']/CRID")
                PTAS = cu.findall('./PTAS/PointAssociation')
                if len(PTAS) == 1:
                    nk = PTAS[0].get('NameKey')
                    for pt in cu.findall('./SEGH/C2IL/Point'):
                        pts.append({'XCOO':float(pt.get('XCOO')), 
                                    'YCOO':float(pt.get('YCOO'))})
                else:
                    for pt in cu.findall('./SEGH/C2IL/Point'):
                        pts.append({'XCOO':float(pt.get('XCOO')), 
                                    'YCOO':float(pt.get('YCOO'))})
        elif primitive == 'Surface':
            su = self.SurfaceRecords.find("./Surface[@NameKey='"+nk+"']/SRID")
            for ri in su.findall('./RIAS/Ring'):
                pass
            
        return pts


if __name__ == '__main__':
    print("s101_reader")

