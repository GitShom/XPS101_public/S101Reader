#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Lancement des fonctions pour tester les fonctionnalités du package


from s101Reader.s101_reader import s101Reader


if __name__ == '__main__':
    print("s101_reader")

    #enc = 'K:/CELLULE_XP/Projet Shom UKHO Dual Fuel/Route et ENC/ENC/ENC FR/FR471550/dKart/ENC_sans_CATCOV2/101FR00471550__.000'
    #enc = 'D:/Nicolas/Python/divergENCes/tmp/101FR0057147A__.000'
    enc = 'D:/Nicolas/VS Code/101FR00471550__.000'


    catalog = 'D:/Nicolas/Python/s101Catalog/data/S-101FC_1.0.1_20210720.xml'

    output = 'D:/Nicolas/Python/tmp/101FR00471550__.xml'
    output_obj = 'D:/Nicolas/Python/tmp/101FR00471550__objects.xml'

    print(f'lecture {enc}')
    s101 = s101Reader(enc, catalog)
    s101.read()

    print('conversion en objets')
    s101.toObjects()

    s101.xml2file(s101.tree, output)
    s101.xml2file(s101.tree_object, output_obj)


    """
    print('conversion en geojson')
    geojson = s101.convert2geojson('Surface')

    #filtered = s101.geojsonFilterByCode(geojson, 'QualityOfBathymetricData')
    #filtered2 = s101.geojsonFilterByCode(geojson, 'QualityOfSurvey')
    filtered3 = s101.geojsonFilterByCode(geojson, ['QualityOfBathymetricData', 'QualityOfSurvey'])

    print('export geojson')
    s101.geojson2file(geojson, 'D:/Nicolas/Python/tmp/101FR00471550__.geojson')

    #s101.geojson2file(filtered, 'D:/Nicolas/Python/tmp/101FR00471550__QOBD.geojson')
    #s101.geojson2file(filtered2, 'D:/Nicolas/Python/tmp/101FR00471550__QOS.geojson')
    s101.geojson2file(filtered3, 'D:/Nicolas/Python/tmp/101FR00471550__filtered.geojson')
    """
    print('conversion en geojson')
    geojson = s101.convert2geojson('Surface')
    filtered3 = s101.geojsonFilterByCode(geojson, ['QualityOfBathymetricData', 'QualityOfSurvey'])
    s101.geojson2file(filtered3, 'D:/Nicolas/Python/tmp/101FR00471550_surface_filtered.geojson')

    geojson = s101.convert2geojson('Point')
    filtered3 = s101.geojsonFilterByCode(geojson, ['Wreck'])
    s101.geojson2file(filtered3, 'D:/Nicolas/Python/tmp/101FR00471550_point_filtered.geojson')

    print('FIN')