from setuptools import setup

setup(
    name='s101Reader',
    version='1.0',
    packages=['s101Reader'],
    url='https://www.shom.fr',
    license='',
    author='Nicolas Gabarron',
    author_email='nicolas.gabarron@shom.fr',
    description='Lecture d\'un fichier S-101'
)
