

# Package Python s101Reader

Ce package permet de lire et extraire des données d'une ENC au format S-101
Ce package est composé de 3 fichiers Python:
- s101_reader.py
- s101_record.py
- s101_field.py

### Les classes

#### *s101Reader*

C'est la classe principale, stockée dans s101_reader.py.

*@url*: chemin du fichier S-101  
*@catalog*: chemin du fichier catalogue S-101

- read()

- validate()

Valide les valeurs/formats des champs

*return*: {'status': status, 'messages': msg}
            status = True/False
            msg = liste des messages d'erreur

- ATCS_Codes()
- ATCS_NumericCodes()
- ITCS_Codes()
- ITCS_NumericCodes()
- FTCS_Codes()
- FTCS_NumericCodes()
- IACS_Codes()
- IACS_NumericCodes()
- FACS_Codes()
- FACS_NumericCodes()
- ARCS_Codes()
- ARCS_NumericCodes()
- toObjects()

- geometry2geojson(geom)

*@geom* : balise géométrie du fichier *xml* des objets

Exemple pour une géométrie *Surface*:
```
<Surface NameKey="1666">
  <SRID RCID="6" RCNM="130" RUIN="1" RVER="1">
    <RIAS>
      <Ring NameKey="24701" ORNT="1" RAUI="1" RRID="96" RRNM="125" USAG="1"/>
    </RIAS>
  </SRID>
</Surface>
```

- object2geojson(nk, geom)

*@nk*: NameKey de l'objet  
*@geom*: balise géométrie de l'objet (Point, CompositeCurve, Surface)

- convert2geojson(geom)

*@geom*:

- attributeCode(num)

*@num*: code numérique de l'attribut  
*return*: libellé de l'attribut

- featureCode(num)

*@num*: code numérique de l'objet  
*return*: libellé de l'objet

- informationCode(num)

*@num*: code numérique de l'information  
*return*: libellé de l'information

- updateCoordinate(cmfx, cmfy, cmfz)

*@cmfx*: facteur multiplicatif pour les longitudes  
*@cmfy*: facteur multiplicatif pour les latitudes  
*@cmfz*: facteur multiplicatif pour l'altitude

- updateAttribute()

Rajoute le code (label) de l'attribut dans toutes les balises *Attribute*

- updateFeature()

Rajoute le code (label) de l'objet dans toutes les balises *FRID*

- updateInformation()

Rajoute le code (label) de l'information dans toutes les balises *IRID*
